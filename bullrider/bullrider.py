#!/bin/python3
import threading
from multiprocessing import Process, Queue
import math
import datetime
import os, sys
from get_all_tickers import get_tickers as gt
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
from stockbaba_lib import get_stock_info_lib
import traceback
import pdb
import json
import time
import glob
import csv
#from pdb_clone import pdbhandler

#GLoabal Variables:
input_args = None
total_num_stocks = 0
workerlist = []
'''
class ForkedPdb(pdb.Pdb):
    """A Pdb subclass that may be used
    from a forked multiprocessing child

    """
    def interaction(self, *args, **kwargs):
        _stdin = sys.stdin
        try:
            sys.stdin = open('/dev/stdin')
            pdb.Pdb.interaction(self, *args, **kwargs)
        finally:
            sys.stdin = _stdin
'''

class worker_forks:
    def __init__(self, ticker_list, name, from_index, to_index, arguements):
        self.name = name
        self.from_ind = int(from_index)
        self.to_ind = int(to_index)
        self.num_bullish_stocks = 0
        self.input_args = arguements
        self.ticker_list = ticker_list
        self.out_filename = "bullrider_output_%s"%(self.name)
        self.json_file_name = "bullrider_json_%s"%(self.name)
        self.json_data = {}
        self.json_data['bullrider'] = []
        self.json_data['total_bullriders'] = []

    #yfinance APIs are not Thread safe, they throw all kinds of exceptions when in multithreaded environment.
    #Will be using multiple forks to isolate the yfinace calls and avoid contention.
    def worker(self):
        sys.stdout = open(self.out_filename, 'w')
        print('Parent process:', os.getppid())
        print('Process id:', os.getpid())
        #ForkedPdb().set_trace()
        for i in range(self.from_ind, self.to_ind):
            print("Downloading info of %s Thread name %s"%(self.ticker_list[i], self.name))
            sys.stdout.flush()
            try:
                stock = get_stock_info_lib.stock_basic_analysis(stock_ticker = self.ticker_list[i], start_d = self.input_args['bullrider_filters']['from_date'], end_d = self.input_args['bullrider_filters']['to_date'], index = i)
            except:
                print("Exception raised for ticker %s"% self.ticker_list[i])
                if stock != None:
                    del stock
                continue
            if stock == None:
                print("Could not find stock %s"%self.ticker_list[i])
                continue
            valid = stock.bullrider_filter_condition(min_price = self.input_args['bullrider_filters']['min_price'], 
                        y_rise_thresold = self.input_args['bullrider_filters']['yearly_rise'], 
                        num_div_thresold = self.input_args['bullrider_filters']['minimum_num_dividends'], 
                        num_splits_threshold = self.input_args['bullrider_filters']['minimum_stock_splits'])
            if valid == True:
                print ("Got valid stock: %s"%self.ticker_list[i])
                stock.stock_details_json_object
                self.json_data['bullrider'].append(stock.stock_details_json_object())
                self.num_bullish_stocks += 1
            else:
                del stock
        print("Total Bullish stocks on %s are %d"%(self.name, self.num_bullish_stocks))
        sys.stdout.flush()
        self.json_data['total_bullriders'].append({'total-bullriders': self.num_bullish_stocks})
        with open(self.json_file_name, 'w') as outfile:
            json.dump(self.json_data, outfile, indent=2)

    #Prepare for Fork Arguements
    def worker_create(self):
        self.fork = Process(target=self.worker, args=())
        if self.fork == None:
            return False
        return True


def open_input_args():
    global input_args
    with open('./input.json') as input_file:
        input_args = json.load(input_file)

def get_bullriders():
    disable_fork = False
    global workerlist
    list_of_tickers = gt.get_tickers()
    #These are the stocks that are good, that I know of and can be used for validatiosn.
    #list_of_tickers = [ "DG", "DECK", "DE"]
    total_num_stocks = len(list_of_tickers)
    start_index = 0
    max_tickers_per_worker = 500
    for i in range(total_num_stocks//max_tickers_per_worker + 1):
        start_index = i*max_tickers_per_worker
        if total_num_stocks < (i+1)*max_tickers_per_worker:
            end_index = total_num_stocks
        else:
            end_index = (i+1)*max_tickers_per_worker

        #Making Worker Variables
        worker_name = "Hunter-%d"%(i)
        worker_c = worker_forks(name = worker_name, ticker_list = list_of_tickers, from_index = start_index, to_index = end_index, arguements = input_args)
        if disable_fork == False:
            if worker_c.worker_create() == False:
                print("%s could not be created. Index %d to %d cannot be preoceesed."%(worker_name, start_index, end_index))
                del worker_c
                continue

        workerlist.append(worker_c)
        if end_index == total_num_stocks:
            break
    
    if disable_fork == True:
        workerlist[i].worker()
    else:
        for i in range(len(workerlist)):
            workerlist[i].fork.start()
        for i in range(len(workerlist)):
            workerlist[i].fork.join()

def collaborate_output():
    '''
    for i in range(len(workerlist)):
        with open(workerlist[i].json_file_name) as out_file:
            result = json.load(out_file)
    '''
    result = []
    final_result = {}
    final_result['bullrider'] = []
    final_result['total_bullrider'] = []
    total_bullriders = int(0)
    for f in glob.glob("bullrider_json_*"):
        with open(f) as out_file:
            result = json.load(out_file)
            for i in range(len(result['bullrider'])):
                final_result['bullrider'].append(result['bullrider'][i])
            total_bullriders += int(result['total_bullriders'][0]['total-bullriders'])
        final_result['bullrider'].sort(key = lambda k: k['yearly_average']['average'][0]['average_rise'], reverse = True)
    final_result['total_bullrider'].append({
        'total_bullriders': total_bullriders
    })
    with open("output.json", "w") as outfile:
        json.dump(final_result, outfile, indent=2)

    #Write to csv file
    field_names=["Ticker", "Company Name", "Sector", "Industry", "Total number of dividends", "Total number of forward Splits", "Yearly Rise Average", "History"]
    with open('output.csv', 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames = field_names)
        writer.writeheader()
        for element in final_result['bullrider']:
            history_buff = ""
            for y_info in element['yearly_average']['yearly_info']:
                history_buff += "%s : %f\n"%(y_info['year_info'], y_info['year_rise'])
            writer.writerow({'Ticker': element['ticker'], 'Company Name': element['company_name'], 'Sector': element['sector'], 'Industry': element['industry'], 
                            'Total number of dividends': element['num_dividends'], 'Total number of forward Splits': element['num_positive_splits'], 
                            'Yearly Rise Average': element['yearly_average']['average'][0]['average_rise'], 'History': history_buff})

if __name__ == '__main__':
    #pdbhandler.register()
    open_input_args()
    get_bullriders()
    collaborate_output()
