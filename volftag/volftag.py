#!/bin/python3
import threading
from multiprocessing import Process, Queue
import math
import datetime
import os, sys
from get_all_tickers import get_tickers as gt
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
from stockbaba_lib import get_stock_info_lib
from stockbaba_lib import stock_news
import traceback
import pdb
import json
import time
import glob
import csv
import shutil
from stockbaba_lib import volftaglib
from stockbaba_lib import volftag_math
#from pdb_clone import pdbhandler

#GLoabal Variables:
input_args = None
total_num_stocks = 0
workerlist = []
volftaglib_t = None

class worker_forks:
    def __init__(self, ticker_list, name, from_index, to_index, arguements):
        self.name = name
        self.from_ind = int(from_index)
        self.to_ind = int(to_index)
        self.input_args = arguements
        self.ticker_list = ticker_list
        self.out_filename = "sentiquity_output_%s"%(self.name)
        self.json_file_name = "sentiquity_json_%s"%(self.name)
        self.json_data = {}
        self.json_data['sentiquity'] = []
        self.json_data['total_sentiquity'] = []
        #The sql server Info of the database
        self.volftaglib = volftaglib.data_lib(  host = input_args['mysql_info']['host'], sql_user = input_args['mysql_info']['user'], sql_passwd = input_args['mysql_info']['password'], 
                                            my_email = input_args['gmail_info']['myemail'], passwd_file= input_args['gmail_info']['password_file'], 
                                            gsheet_scope = input_args['gsheet_info']['scope'], gsheet_id = input_args['gsheet_info']['sheet_id'], range_capture = 'Form Responses 1!A:Z')


    #yfinance APIs are not Thread safe, they throw all kinds of exceptions when in multithreaded environment.
    #Will be using multiple forks to isolate the yfinace calls and avoid contention.
    def worker(self):
        #sys.stdout = open(self.out_filename, 'w')
        print('Parent process:', os.getppid())
        print('Process id:', os.getpid())
        #ForkedPdb().set_trace()

        for i in range(self.from_ind, self.to_ind):
            print("Downloading info of %s Thread name %s"%(self.ticker_list[i], self.name))
            sys.stdout.flush()
            try:
                stock = get_stock_info_lib.stock_basic_analysis(stock_ticker = self.ticker_list[i], start_d = "2021-09-04", end_d = "2021-09-05", interval_a = "1m", index = i, auto_adjust = True)
            except:
                print("Exception raised for ticker %s"% self.ticker_list[i])
                continue
            if not stock:
                continue
            
            print("Price\t\tVolume");
            for i in range(len(stock.data.Open)):
                if math.isnan(stock.data.Open[i]):
                    continue
                print("%s\t\t%.2f\t\t%d"%(str(stock.data.High._index[i]), (stock.data.High[i] + stock.data.Low[i])/2, stock.data.Volume[i]))
                
        sys.stdout.flush()
        

    #Prepare for Fork Arguements
    def worker_create(self):
        self.fork = Process(target=self.worker, args=())
        if self.fork == None:
            return False
        return True


def open_input_args():
    global input_args
    with open('./input.json') as input_file:
        input_args = json.load(input_file)

def clear_output():
    if not os.path.exists('output'):
        os.makedirs('output')
    else:
        shutil.rmtree('output')
        os.makedirs('output')

def update_the_sheets_and_customer_table(input_args):
    global volftaglib_t
    volftaglib_t = volftaglib.data_lib(  host = input_args['mysql_info']['host'], sql_user = input_args['mysql_info']['user'], sql_passwd = input_args['mysql_info']['password'],
                                my_email = input_args['gmail_info']['myemail'], passwd_file= input_args['gmail_info']['password_file'],
                                gsheet_scope = input_args['gsheet_info']['scope'], gsheet_id = input_args['gsheet_info']['sheet_id'], 
                                range_capture = 'Form Responses 1!A:Z')
    volftaglib_t.check_new_entry_in_sheets()

def get_sentiquity():
    disable_fork = True 
    global workerlist
    global input_args
    global volftaglib_t
    #list_of_tickers = gt.get_tickers()
    #These are the stocks that are good, that I know of and can be used for validatiosn.
    start_index = 0
    update_the_sheets_and_customer_table(input_args)
    list_of_tickers = volftaglib_t.get_the_final_list_of_tickers()
    total_num_stocks = len(list_of_tickers)
    del volftaglib_t

    max_tickers_per_worker = 500
    for i in range(total_num_stocks//max_tickers_per_worker + 1):
        start_index = i*max_tickers_per_worker
        if total_num_stocks < (i+1)*max_tickers_per_worker:
            end_index = total_num_stocks
        else:
            end_index = (i+1)*max_tickers_per_worker

        #Making Worker Variables
        worker_name = "Hunter-%d"%(i)
        worker_c = worker_forks(name = worker_name, ticker_list = list_of_tickers, from_index = start_index, to_index = end_index, arguements = input_args)
        if disable_fork == False:
            if worker_c.worker_create() == False:
                print("%s could not be created. Index %d to %d cannot be preoceesed."%(worker_name, start_index, end_index))
                del worker_c
                continue

        workerlist.append(worker_c)
        if end_index == total_num_stocks:
            break
    
    if disable_fork == True:
        workerlist[i].worker()
    else:
        for i in range(len(workerlist)):
            workerlist[i].fork.start()
        for i in range(len(workerlist)):
            workerlist[i].fork.join()

def print_zero_analytics(stock, threshold_samples):
    stock.analyze_zero_merics(threshold_samples)
    print("# of Samples: %d"%len(stock.data))
    print("# Continous Zeros: %d"%(stock.num_neg_samples + stock.num_positive_samples))
    print("# Continuous Zeros caused Price increase: %d"%stock.num_neg_samples)
    print("# Continuous Zeros caused Price Decrease: %d"%stock.num_positive_samples)
    print("# Zero Decrement Samples when price dropped: %d"%stock.num_z_decs)
    print("# Zero Decrement Samples when price increased: %d"%stock.num_z_incr)
    print("Average number of zeros that caused price drops: %d"%(stock.num_z_decs/stock.num_positive_samples))
    print("History when Price Increased")
    tmp_avg = 0
    tmp_vol = 0
    tmp_vol_counter = 0
    for ele in stock.neg_sample_history:
        tmp_avg = tmp_avg + ele[2]
        if ele[5] < 1000000000:
            tmp_vol = tmp_vol + ele[5]
            tmp_vol_counter = tmp_vol_counter + 1
        print(ele)
    if tmp_vol_counter > 0:
        print("\nAverage Increment : %f , Average Pre Zero Volume : %s"%(tmp_avg/len(stock.neg_sample_history), stock.get_int_to_str_mill(tmp_vol/tmp_vol_counter)))

    print("=================History when Price Decreased================")
    tmp_avg = 0
    tmp_vol = 0
    tmp_vol_counter = 0
    for ele in stock.pos_sample_history:
        tmp_avg = tmp_avg + ele[2]
        if ele[5] < 1000000000:
            tmp_vol = tmp_vol + ele[5]
            tmp_vol_counter = tmp_vol_counter + 1
        print(ele)
    print("\nAverage Decrement : %f , Average Pre Zero Volume : %s"%(tmp_avg/len(stock.pos_sample_history), stock.get_int_to_str_mill(tmp_vol/tmp_vol_counter)))

    print("============================================================")
    for i in range(len(stock.data)):
        print("%s %f %s"% (stock.data.Open._index[i], stock.avg_price[i], stock.get_int_to_str_mill(stock.data.Volume[i])))


def get_an_algo_for_prediction(stock, threshold, min_diff = 0, money_gained = True):
    amount = 0
    if money_gained == True:
        list_name = stock.vol_incr_history
    else:
        list_name = stock.vol_decr_history

    for ele in list_name:
        weight = 0
        slope_prev_20 = float(ele[-1])
        first_sample_set_positive_outcomes = ele[7]
        consider = (ele[-2] != 'Decreasing') or (first_sample_set_positive_outcomes >= threshold)
        #if slope_prev_20 >= 0.58:
        #    weight += 0.25
        if ele[-2] == 'Increasing':
            weight += 0.5
        if (first_sample_set_positive_outcomes >= threshold):
            weight += 0.25
        if weight == 0.75:
            if money_gained == True:
                amount += (ele[4] - 6)
            else:
                amount += (ele[4] + 6)
            print(ele[4])
    return amount

def get_order_amount_gained(buy_price, sell_price, order_fees, short = False):
    if short == False:
        if buy_price > sell_price:
            return (buy_price - sell_price + order_fees)*-1
        else:
            return sell_price - buy_price - order_fees

def get_an_algo_for_prediction_2(stock, threshold, money_gained = True, order_fees = 6.0, loss_limit_order = 20):
    amount = 0
    if money_gained == True:
        list_name = stock.vol_incr_history
    else:
        list_name = stock.vol_decr_history
        order_fees = order_fees * -1

    prev_prediction_index = 0
    for ele in list_name:
        from_i = ele[1]
        max_index = ele[2]
        if from_i < prev_prediction_index:
            continue

        to_i = from_i + threshold + int(threshold/2)
        order_placed = 0
        crossed_thresh = ele[6]
        if crossed_thresh < 3:
            continue
        while to_i < len(stock.data):
            if to_i > max_index:
                if order_placed == 1:
                    amount_earned = get_order_amount_gained(stock.avg_price[order_index], stock.avg_price[to_i], order_fees)
                    print("Order Buy index %d at %f, Sell index %d at %f. Amount: %f"%(order_index, stock.avg_price[order_index], to_i, stock.avg_price[to_i], amount_earned))
                    amount += amount_earned
                break
            if order_placed == 0:
                order_placed = 1
                order_index = to_i
            else:
                if (stock.data.High[to_i] < (stock.avg_price[order_index] - loss_limit_order) ) or \
                        (stock.data.Low[to_i] < (stock.avg_price[order_index] - loss_limit_order)):
                    amount_earned = get_order_amount_gained(stock.avg_price[order_index], stock.avg_price[to_i], order_fees)
                    print("Order Buy index %d at %f, Sell index %d at %f. Amount: %f"%(order_index, stock.avg_price[order_index], to_i, stock.avg_price[to_i], amount_earned))
                    amount += amount_earned
                    break
            to_i += 1
    print("Total Amount Earned: %f"%amount)
   
#Given the interval, get the number of samples that combines to 1 day
def get_interval_to_moving_samples(interval, days):
    if interval == "1m":
        return 1440*days
    elif interval == "15m":
        return 96*days
    elif interval == "30m":
        return 48*days
    elif interval == "1h":
        return 24*days
    elif interval == "2h":
        return 12*days
    elif interval == "3h":
        return 8*days
 
def print_volume_analytics(stock, interval):
    threshold = 2
    stock.check_if_volume_constant(threshold, moving_samples = get_interval_to_moving_samples(interval, 1))
    print("# of Samples: %d, Average Volume %s"%(len(stock.data), stock.get_int_to_str_mill(stock.get_average_volume(0, len(stock.data)))))
    print("# of times Price increased when Volume increased above avg: %d"% stock.num_vol_incr)
    print("# of times Price decreased when Volume decreased above avg: %d"% stock.num_vol_decr)
    print("=================History when Price Incresed when positive Volumes occured================")
    for ele in stock.vol_incr_history:
        print(ele)
        for i in range(ele[1], ele[2]):
            print("%f\t%f\t%f\t%f\t%s"%(stock.data.Open[i], stock.data.High[i], stock.data.Low[i], stock.data.Close[i], stock.get_int_to_str_mill(stock.data.Volume[i])))
    #print("Algo Earnt  with Success: %f"%(get_an_algo_for_prediction(stock, threshold/2 )))
    print("============ Algo 2 Positive Volumes ==============")
    get_an_algo_for_prediction_2(stock, threshold)

    print("=================History when Price Decresed when positive Volumes occured================")
    for ele in stock.vol_decr_history:
        print(ele)
        for i in range(ele[1], ele[2]):
            print("%f\t%f\t%f\t%f\t%s"%(stock.data.Open[i], stock.data.High[i], stock.data.Low[i], stock.data.Close[i], stock.get_int_to_str_mill(stock.data.Volume[i])))
    #print("Algo Lost  with Misprediction: %f"%(get_an_algo_for_prediction(stock, threshold/2, money_gained = False)))
    print("============ Algo 2 Negative Volumes ==============")
    get_an_algo_for_prediction_2(stock, threshold, money_gained = False, order_fees = -6.0)

def print_all_values(stock):
    print("=====================The values of all the stocks========================")
    for i in range(len(stock.data)):
        print("%d\t\t%s\t\t%f\t\t%s"%(i, str(stock.data.Open._index[i]), stock.avg_price[i], stock.get_int_to_str_mill(stock.data.Volume[i])))


def print_candlestick_info(stock):
    print("=====================Candle Analysis========================")
    for i in range(len(stock.data)):
        entry_made = 0
        for candle in stock.candle_names:
            if stock.data[candle][i] != 0:
                if entry_made == 0:
                    print("%d\t\t%s\t\t%f\t\t%s\t\t%s=%d"%(i, str(stock.data.Open._index[i]), stock.avg_price[i], stock.get_int_to_str_mill(stock.data.Volume[i]), candle, stock.data[candle][i]), end='')
                    entry_made = 1
                else:
                    print("\t\t%s=%d"%( candle, stock.data[candle][i]), end='')
        if entry_made == 1:
            print("")

def check_math_library():
    stocklist = ["ETH-USD"]
    from_date = "2021-10-04"
    end_date = "2021-10-14"
    sample_time = "1h"
    for ele in stocklist:
        threshold_samples = 3
        stock = volftag_math.volftagmath(ticker = ele, price_threshold = 0)
        stock.get_stock_info(start_d = from_date, end_d = end_date, interval_a = sample_time, price_type = 1)
        print_zero_analytics(stock, threshold_samples)
        print_volume_analytics(stock, sample_time)
        print_all_values(stock)
        print_candlestick_info(stock)
        

if __name__ == '__main__':
    check_math_library()
