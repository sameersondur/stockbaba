/*
    This is always a synchronous API call
    method : GET,POST,,,
    url : You know it
    req_header : [{
                    'type': 'Content-type', 
                    'data': ;application/x-www-form-urlencoded'
                  } ]
*/

console.log("Request Failed");
function send_request(method, url, debug = false, user = null, password = null, responsetype = "", req_header = [], send_params = null) {
    var xmlhttp = new XMLHttpRequest();
    if (responsetype != "") {
        xhr.responseType = responsetype;
    }
    for(let i = 0; i < req_header.length; i++) {
        xhr.setRequestHeader(req_header[i].type, req_header[i].data);
    }
    xmlhttp.open(method, url, false, user, password);
    xmlhttp.onload = function(){
        if(xmlhttp.status ==200){
            if (debug)  {
                console.log(xmlhttp.responseText);
            }
            return [xmlhttp.status, xmlhttp.responseText];
        } else {
            if (debug)  {
                console.log("Request Failed");
            }
            return [null, null];
        }
    }
    xmlhttp.send(send_params);
    return xmlhttp.onload();
}
