import mysql.connector
import traceback

class database():
    def exe_handler(self, e):
        print("Mysql Exception")
        print(e)
        traceback.print_exc()

    def __init__(self, host, user, password, debug = True):
        self.host = host
        self.user = user
        self.password = password
        try:
            self.connection = mysql.connector.connect(
            host="localhost",
            user=user,
            password=password
            )
        except Exception as e:
            self.exe_handler(e=e)
            return None
        self.cursor = self.connection.cursor()
        self.db_name = None
        self.debug = debug

    def sqlexecute(self, command, var_arr = None):
        if self.debug == True:
            if var_arr == None:
                print("MYSQL command for Database %s: %s"%(self.db_name, command))
            else:
                buff = "MYSQL command for Database %s: "%self.db_name + "Command: " + command + " Variables: " + ", ".join(str(elem) for elem in var_arr)
                print(buff)
        self.cursor.reset()
        if var_arr == None:
            self.cursor.execute(command)
        else:
            self.cursor.execute(command, var_arr)

    def create(self, d_name):
        try:
            self.sqlexecute("CREATE DATABASE IF NOT EXISTS %s"%d_name)
        except Exception as e:
            self.exe_handler(e=e)
            return None

    def list_all_databases(self):
        self.sqlexecute("SHOW DATABASES")
        print("This is the list of all databases")
        for x in self.cursor:
            print(x)
        
    def enter_database_context(self, dbase_name):
        self.db_name = dbase_name
        try:
            self.sqlexecute("USE %s"%dbase_name)
        except Exception as e:
            self.exe_handler(e=e)
            self.db_name = None
            return None

    def create_table(self, table_string):
        if self.check_if_table_exists(table_name = table_string.split()[0]) == True:
            return
        table_create_cmd = "CREATE TABLE "+table_string
        self.sqlexecute(table_create_cmd)

    def check_if_table_exists(self, table_name):
        self.sqlexecute("SHOW TABLES")
        for x in self.cursor:
            if x[0] == table_name:
                return True
        return False

    def list_all_elements_in_table(self, table):
        cmd = "SELECT * FROM %s "%table
        self.sqlexecute(cmd)
        print("The elemets in the table %s are:"%table)
        for x in self.cursor:
            print(x)

    def get_all_elements_in_table(self, table):
        cmd = "SELECT * FROM %s "%table
        self.sqlexecute(cmd)
        ret = []
        for x in self.cursor:
            ret.append(list(x))
        return ret

    def check_if_val_present(self, table, key, value):
        cmd = "SELECT * FROM %s WHERE %s = %s"%(table, key, value)
        self.sqlexecute(cmd)
        result = []
        for x in self.cursor:
            result.append(x)
        if self.debug == True:
            print("Lenght %d"%len(result))

        if len(result) > 0:
            return True
        return False

    def get_table_entry(self, table, key, value):
        cmd = "SELECT * FROM %s WHERE %s = %s"%(table, key, value)
        self.sqlexecute(cmd)
        result = []
        for x in self.cursor:
            result.append(x)
        return result
        

    def list_all_tables(self):
        self.sqlexecute("SHOW TABLES")
        print("The tables ")
        for x in self.cursor:
            print(x)

    '''
        If the values are strings, then they should be wrapped around single quotes.
        Ex: if primary_val is string, then it should be primary_val= "\'blahblah\'"
    '''
    def insert_into_table(self, command_string, val_arr, table, primary_key, primary_val):
        if self.check_if_val_present(table = table, key = primary_key, value = primary_val) == True:
            return False
        cmd = "INSERT INTO %s"%command_string
        self.sqlexecute(command = cmd, var_arr = val_arr)
        self.connection.commit()
        return True

    '''
        If the values are strings, then they should be wrapped around single quotes.
        Ex: if primary_val is string, then it should be primary_val= "\'blahblah\'"
        keys : It is a list of keys to be updated
        values : It is a list of values to be updated.
    '''
    def update_table_values(self, table, keys, values, primary_key, primary_val):
        cmd = "UPDATE %s SET "%table
        if len(values) == 0 or len(keys) == 0 or len(values) != len(keys):
            return False
        cmd_val = ""
        for i in range(len(keys)):
            if i > 0:
                cmd_val = cmd_val + ","
            cmd_val = cmd_val + "%s = %s "%(keys[i], values[i])
        cmd = cmd + cmd_val
        cmd = cmd + "WHERE %s = %s"%(primary_key, primary_val)
        try:
            self.sqlexecute(cmd)
            self.connection.commit()
            return True
        except Exception as e:
            self.exe_handler(e=e)
            return False
        
    def delete_table_rows(self, table, primary_key, primary_value):
        cmd = "DELETE FROM %s WHERE %s = %s "%(table, primary_key, primary_value)
        try:
            self.sqlexecute(cmd)
            self.connection.commit()
            return True
        except Exception as e:
            self.exe_handler(e=e)
            return False


    def __del__(self):
        del self.host
        del self.user
        del self.password
        #self.cursor.close()
        self.connection.close()
