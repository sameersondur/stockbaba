Buy Surging Deere Stock for Strong 2021 Outlook?
Deere & Company DE shares have left its industry and the S&P 500 in the dust in 2020, with the stock up over 45%. The farm equipment firm’s results steadily improved over the past three quarters and DE executives project increased demand in 2021.
Improving Farming Outlook?
DE’s sales have slipped in five out of the last six quarters, but its top-line declines slowly shrunk. For example, its Q2 sales fell by 18%, with Q3 down 11%, and its fourth quarter revenue down only 1.7% from the year-ago period. More importantly, the firm raised its FY21 outlook at the end of November when it reported its Q4 results.
Executives at one of the world’s largest farm equipment companies said they expect higher crop prices will help boost DE’s bottom-line next year. The logic is that the higher prices will lift demand for its tractors and combines from U.S. farmers. “Things have improved from a commodity price perspective, lower stock levels, that's all really happened very, very recently,” Director-Investor Relations at DE Joshua Jepsen said on the company’s Q4 earnings call.
 
 
 
 
 
 
 
 
 
 
 
What’s Next?
The opening line from a mid-November Wall Street Journal article helps summarize why Deere and U.S. farmers might be excited as we headed into 2021: “Dry weather, China’s push to fatten its pigs and the lockdown-induced baking bonanza are lifting prices for U.S. row crops.” With this in mind, Zacks estimates call for DE’s Q1 sales to pop 8%, with Q2 projected to climb 17%.
These estimates come against easier to compare periods, but they mark improvements nonetheless. Overall, Deere’s fiscal 2021 revenue is projected to climb 11.5% to $34.9 billion, with FY22 projected to come in another 9% higher to reach $37.9 billion. Meanwhile, its adjusted EPS figures are expected to surge 46% and 21%, respectively during this stretch.   
Analysts have raced to lift their bottom-line estimates for DE since its Q4 report, as the nearby chart showcases. Deere has also topped our EPS estimates by an average of over 50% in the trailing four periods.
 
 
 
 
 
 
 
 
 
 
Bottom Line 
DE’s earnings revisions activity helps it grab a Zacks Rank #1 (Strong Buy) right now. The stock is also part of the Manufacturing-Farm Equipment space that rests in the top 2% of our over 250 Zacks industries at the moment, alongside other highly-ranked stocks such as AGCO Corporation AGCO, Alamo Group, Inc. ALG, and others.
Investors should remember that Deere has easily outpaced the S&P 500 and Caterpillar CAT over the last five years and over the past 12 months, up 50% vs. CAT’s 25% and the index’s 18%. DE’s 1.2% dividend yield, which outpaces the 10-year U.S. treasury, provides an added boost. The stock also trades at a discount compared to its industry and the S&P 500, and it closed regular trading Tuesday 5% off its November highs.
Biggest Tech Breakthrough in a GenerationBe among the early investors in the new type of device that experts say could impact society as much as the discovery of electricity. Current technology will soon be outdated and replaced by these new devices. In the process, it’s expected to create 22 million jobs and generate $12.3 trillion in activity.A select few stocks could skyrocket the most as rollout accelerates for this new tech. Early investors could see gains similar to buying Microsoft in the 1990s. Zacks’ just-released special report reveals 8 stocks to watch. The report is only available for a limited time.See 8 breakthrough stocks now>>
Want the latest recommendations from Zacks Investment Research? Today, you can download 7 Best Stocks for the Next 30 Days. Click to get this free report Deere & Company (DE) : Free Stock Analysis Report AGCO Corporation (AGCO) : Free Stock Analysis Report Caterpillar Inc. (CAT) : Free Stock Analysis Report Alamo Group, Inc. (ALG) : Free Stock Analysis Report To read this article on Zacks.com click here. Zacks Investment Research