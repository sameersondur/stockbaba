
Dow Jones futures rose modestly Monday afternoon, along with S&P 500 futures and Nasdaq futures, after U.S. markets were closed for Labor Day. The stock market rally had another solid week, with growth and small-cap stocks leading once again. The S&P 500 index and Nasdaq composite hit record highs.
Negative Tesla (TSLA) headlines are building up on expanding crash probes and product delays, but Tesla stock held its buy point Friday.
PayPal (PYPL), Amazon.com (AMZN), Lululemon Athletica (LULU), Nike (NKE) and Applied Materials (AMAT) are all finding support near their 50-day moving averages, offering potential buying opportunities.
Tesla stock and PayPal are on IBD Leaderboard. PayPal stock also is on IBD Long-Term Leaders.
The video embedded in this article analyzes DocuSign (DOCU), PayPal and Lululemon stock.
Get these newsletters delivered to your inbox & more info about our products & services. Privacy Policy & Terms of Use
Ryanair (RYAAY), Boeing's top non-U.S. customer, said it is walking away from talks with the Dow Jones aerospace giant over a potential new 737 Max order over pricing. Meanwhile, 787 Dreamliner deliveries will likely remain halted until at least late October Boeing (BA) has been unable to persuade FAA to approve its proposal to inspect the aircraft, the Wall Street Journal reported, citing sources.
In S&P index news, Match Group (MTCH), Ceridian (CDAY) and Brown & Brown (BRO) will move from the S&P MidCap 400 to the S&P 500 before the open on Monday, Sept. 20. Tandem Diabetes (TNDM) will be added to the S&P MidCap 400. Meanwhile, Microstrategy (MSTR) is being dropped from the S&P SmallCap 600.
Dow Jones futures rose 0.1% vs. fair value. S&P 500 futures advanced 0.1% and Nasdaq 100 futures climbed 0.2%.
Dow Jones futures are trading normally, but U.S. stock exchanges were closed Monday for the Labor Day holiday.
Remember that overnight action in Dow futures and elsewhere doesn't necessarily translate into actual trading in the next regular stock market session.
Join IBD experts as they analyze actionable stocks in the stock market rally on IBD Live
Coronavirus cases worldwide reached 221.94 million. Covid-19 deaths topped 4.58 million.
Coronavirus cases in the U.S. have hit 40.86 million, with deaths above 666,000.
New U.S. cases have leveled off in the past few days, as the wave shifts from many Southern states to further north and west.
The stock market rally had another positive week, even though the major indexes were technically mixed.
The Dow Jones Industrial Average dipped 0.2% in last week's stock market trading. The S&P 500 index edged up 0.6%. The Nasdaq composite climbed 1.5% after jumping 2.8% in the prior week. The small-cap Russell 2000 gained 0.7% after surging just over 5% in the week before.
But it was among growth stocks that the market rally really shined. The Innovator IBD 50 ETF (FFTY) popped 5.4% to a record high, clearing a long consolidation after running nearly 6% in the prior week. The Innovator IBD Breakout Opportunities ETF (BOUT) gained 1.7% last week.
The iShares Expanded Tech-Software Sector ETF (IGV) advanced 1%. The VanEck Vectors Semiconductor ETF (SMH) climbed 0.5%.
Reflecting more-speculative story stocks, ARK Innovation ETF (ARKK) rallied 2.5% and ARK Genomics ETF (ARKG) 2.8%. ARKK moved above its 50-day and 200-day lines while ARKG reclaimed its 50-day. Tesla stock is the No. 1 holding across ARK Invest ETFs.
Other sectors were generally lower.
SPDR S&P Metals & Mining ETF (XME) eked out a 0.3% gain, though many steelmakers had rough weeks. The Global X U.S. Infrastructure Development ETF (PAVE) retreated 1.7%. U.S. Global Jets ETF (JETS) sank 2.2%. SPDR S&P Homebuilders ETF (XHB) edged down 0.4%. The Energy Select SPDR ETF (XLE) retreated 1.4% and the Financial Select SPDR ETF (XLF) 2.3%
Five Best Chinese Stocks To Watch
Tesla CEO Elon Musk confirmed last week that the long-awaited Roadster will be pushed back, again, to at least 2023. Thursday night, Musk reportedly said that the Tesla Cybertruck won't be out until late 2022, with volume production not until late 2023. Those delays strongly suggest 4680 battery cells won't be ready for at least a year as well. The 4680 batteries, assuming they live up to Musk's promises, are key to making the Roadster, Cybertruck and Semi vehicles viable.
So in 2022, Tesla will have two new plants, but no new products or big new markets. It also means that the Cybertruck will enter the market after the Rivian R1T, the GM Hummer and Ford F-150 Lightning.
Meanwhile, Tesla Autopilot probes expanded.  The National Highway Traffic Safety Administration issued a letter to Tesla, demand a wealth of data as part of its probe of 12 Tesla Autopilot crashes into first responder vehicles parked on the side of roads.  Safety regulators also will probe a Tesla crash in New York killing a person changing a tire.
Why This IBD Tool Simplifies The Search For Top Stocks
So, Tesla Inc. faced a bumpy road. But Tesla stock? Shares rose 3% to 733.57, just above an aggressive 730 buy point. Tesla stock moved into a buy zone Monday with a 2.7% gain. After the Cybertruck delay news, TSLA stock dipped below the 730 level, hitting 724.20 intraday but rallied for a 0.2% gain after ARK Invest's Cathie Wood reiterated her $3,000 price target.
More broadly, Tesla stock has been finding support at its rising 200-day line since mid-May. While it isn't a market leader right now, it's been slightly outpacing the S&P 500 index for the last several months.
Amazon stock popped 3.8% to 3,478.05 last week after a 4.7% jump in the prior week. AMZN stock has now reclaimed its 50-day line as builds the right side of a new base with a 3,773.18 buy point, according to MarketSmith analysis. Shares have been rangebound for at least the past year.
A rebound from the 50-day line would offer a buying opportunity, using Wednesday's high of 3,527 as an entry.
Applied Materials stock dipped 0.5% last week to 135.83, but did rise 1% Friday to end the week a fraction above its 50-day line. That also follows a 7.35% spike in the prior week. A rebound from the 50-day line would offer an early entry for AMAT stock, using handle-like mini-consolidation high of 137.89 as an entry.
Applied Materials stock had flirted with buy points at the start of August, but then sold off hard as memory chip demand concerns weighed on memory-exposed semiconductor plays.
Fellow chip-equipment makers Lam Research (LRCX) and Entegris (ENTG) also are showing similar chart patterns to AMAT stock.
PayPal stock has bounced back following mixed results in late July, rising 3.9% to 289.13 last week, regaining its 50-day line. PYPL stock formed a new flat base with a 310.26 buy point. A strong move from current levels could offer an early entry or a good place to start or add a position as a Long-Term Leader. Investors could use 292.65, Tuesday's high as the entry point.
LULU stock had a rough week, falling 4.35% to 388.83, closing slightly below its 50-day and 10-week lines. The reversal from record highs came on higher volume, not a good sign. A lot of retailers have struggled recently, with Covid revival taking a toll. A rebound from the 50-day line could provide an entry – but investors should wait until after Wednesday night's earnings.
Nike stock has pulled back to the 50-day line after a huge earnings gap on June 25 and solid gains into early August. Shares fell 2.6% to 163.29. It was the fourth straight weekly decline for NKE stock, but the retreat came on lighter volume.
Nike stock could move on Lululemon earnings, though it hasn't reacted much to apparel, shoe or sporting goods retailers or makers. Nike earnings are due later this month.
The market rally had another solid week, with Apple, growth stocks and small caps leading the way. Much of the gains came on Monday, with Apple's surge. But growth stocks were strong all week. The Russell 2000 had another strong week, as market breadth showed significant improvement. The Dow fell slightly.
The real strength has been in growth stocks. The FFTY index has had two strong back-to-back gains to record highs after months of choppy action. For growth investors, the past several months have had a lot of ups and downs, with stocks luring traders in and shaking them out.
Not only have growth stocks been in favor, but the trend has continued for more than two weeks, which is actually saying something for 2021.
Think of a stock market rally as a loaf of bread. The "heels" of a rally aren't much fun. Picking the market bottom and top is essentially impossible, so you have to make your bread from the rest of the loaf.
If the broader market or specific sector only trends higher for a few days — you're not going to capture all of that gain — then it's very hard to make headway. But having a sustained run, even for a couple of weeks, makes it's  so much easier. It's the difference between having one piece of bread between the heels or several slices.
Time The Market With IBD's ETF Market Strategy
Growth stocks have been looking great. Investors hopefully added exposure to tech and growth names over the past two weeks or so. A lot of these stocks are now extended, though many names like PayPal, Amazon or AMAT stock are trying to rebound from 50-day lines. Tesla stock is still at a buy point.
It wouldn't be surprising if growth stocks overall pause for a time. So keep a close eye on housing, financial and commodity-related plays hovering near buy points. Look for early entries to protect yourself against sector or market pullbacks.
Review your portfolio. Do you have laggards weighing on your portfolios. Do you take some profits in big recent winners or let them ride?
Read The Big Picture every day to stay in sync with the market direction and leading stocks and sectors.
Please follow Ed Carson on Twitter at @IBD_ECarson for stock market updates and more.
YOU MAY ALSO LIKE:
Want To Get Quick Profits And Avoid Big Losses? Try SwingTrader
Best Growth Stocks To Buy And Watch
IBD Digital: Unlock IBD's Premium Stock Lists, Tools And Analysis Today
Five E-Commerce Plays Near Buy Points