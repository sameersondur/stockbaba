#!/bin/python3
import math
import datetime
import os, sys
import traceback
import pdb
import json
import time
import glob
from pandas_datareader import data as pdr
import pandas as pd

sys.path.append(os.environ["POLYGON_LIB_DIR"])
import polylibs
import datetime
import pickle

class data_file():
    def __init__(self, stock_list, start, end, d_start, d_end, time_gap, time_unit, threads, data_dir = "./stock_data", data_file = "stock_file", list_file = "stock_file_list", min_price = 2):
        self.list = stock_list
        self.start = start
        self.end = end
        self.d_start = d_start
        self.d_end = d_end
        self.time_gap = time_gap
        self.time_unit = time_unit
        self.threads = threads
        self.final_list = []
        self.min_price = min_price
        self.min_trading = 50000

        if os.path.isdir(data_dir) == False:
            mode = 0o666
            os.mkdir(data_dir, mode)
        self.filename = "%s/%s"%(data_dir, data_file)
        self.list_file = "%s/%s"%(data_dir, list_file)

        self.poly = polylibs.polygon_lib(apikey = os.environ["POLYGON_IO_API_KEY"], debug = False)
        if os.path.isfile(self.filename) == False:
            self.p_list = self.get_data_from_server(self.start, self.end, "30", "minute")
            self.d_list = self.get_data_from_server(self.d_start, self.d_end, "1", "day")
            self.filter_by_price_volume()
            self.save_to_file()
        else:
            self.load_from_file()
            '''
            if self.check_if_tickers_in_data() == False:
                os.remove(self.filename)
                del self.p_list
                self.p_list = self.get_data_from_server()
                self.save_to_file()
            else:
            '''
            print("Retrieved the Info from File %s"%self.filename)
        print("Got the data for list from %s to %s"%(self.final_list[0], self.final_list[-1]))
        
    def get_final_list(self):
        self.final_list = self.list
        #for tick in self.list:
        #    if tick in self.p_list['Open']:
        #        self.final_list.append(tick)

    def is_ticker_low_trading(self, close, volume):
        total_strength = 0.0
        samples = 10
        if len(close) < 10:
            samples = len(close)
        index = -1
        for x in range(samples):
            total_strength += close[index]*volume[index]
            index -= 1

        total_strength = total_strength/samples
        if total_strength >= self.min_trading:
            return True
        return False
           
    def filter_by_price_volume(self):
        for ele in self.list:
            if self.p_list[ele] is None or 'Close' not in self.p_list[ele] or len(self.p_list[ele]['Close']) == 0:
                continue
            if self.p_list[ele]['Close'][-1] > self.min_price and self.is_ticker_low_trading(self.p_list[ele]['Close'], self.p_list[ele]['Volume']) == True:
                self.final_list.append(ele)
            else:
                del self.p_list[ele]
 
    def check_if_tickers_in_data(self):
        for tick in self.list:
            if "-" in tick:
                continue
            if tick not in self.p_list['Open']:
                return False
        return True

    def check_if_full_data(self):
        first_ele = self.list[0]
        end_date = str(self.p_list['Open'][first_ele]._index[-1])
        end_date = datetime.datetime.strptime(end_date, '%y-%m-%d')

        if end > end_date:
            return False
        return True

    def get_data_from_server(self, start, end, time_gap, time_unit):
        pandas_list = self.poly.fetch_all_info(self.list, start, end, time_gap, time_unit)
        return pandas_list
            
    def save_to_file(self):
        print("Length of the final list: %d"%(len(self.final_list)))
        with open(self.filename, 'wb') as handle:
            pickle.dump([self.p_list, self.d_list], handle, protocol=pickle.HIGHEST_PROTOCOL)
        with open(self.list_file, 'wb') as f:
            pickle.dump(self.final_list, f)

    def load_from_file(self):
        self.p_list, self.d_list = pd.read_pickle(self.filename)
        with open(self.list_file, 'rb') as f:
            self.final_list = pickle.load(f)
                    
