#!/bin/python3
import threading
from multiprocessing import Process, Queue
import math
import datetime
import talib as tb
import os, sys
import traceback
import pdb
import json
import time
import glob
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
from stockbaba_lib import volftaglib
from stockbaba_lib import volftag_math
from datetime import datetime

MACD_CROSSING_NOTHING = 0
MACD_CROSSING_UPWARDS = 1
MACD_CROSSING_DOWNWARDS = 2
MACD_CROSSED_UP_ALREADY = 3
MACD_CROSSED_DOWN_ALREADY = 4

class macd_info():
    def __init__(self, fastperiod=12, slowperiod=26, signalperiod=9):
        self.fastperiod = fastperiod
        self.slowperiod = slowperiod
        self.signalperiod = signalperiod
        self.macd = None
        self.signal = None
        self.hist = None

class analytics():
    def __init__(self):
        self.total_profit = 0
        self.total_loss = 0
        self.net_profit = 0
        self.avg_profit_perc = 0
        self.avg_loss_perc = 0
        self.neg_samples = 0
        self.num_samples = 0
        self.num_more_than_threshold = 0
        self.avg_num_profit_cross_threshold = 0
        self.up_crossings = []
        self.up_cross_max_min = []

class stock_info:
    def __init__(self, ticker, Open, High, Low, Close, Volume, d_open, d_high, d_low, d_close, price_type = 'Close'):
        self.Open = Open
        self.High = High
        self.Low = Low
        self.Close = Close
        self.Volume = Volume
    
        self.ticker = ticker
        self.price_type = price_type
        self.d_open = d_open
        self.d_high = d_high
        self.d_low = d_low
        self.d_close = d_close
        self.setup_data()

        self.macd = macd_info()
        self.daily_macd = macd_info()
        self.rsi = None
        self.bullstics = analytics()
        self.bearstics = analytics()
        self.prev_index = 0
        self.mail_added = 0
        self.mail_message = ""
        self.mail_str = ""
        self.init_done = 0
        self.last_noted_time = ""
        self.rsi = None
        self.adx = None
        self.plus_di = None
        self.minus_di = None

    def setup_data(self):
        if self.price_type == 'Close':
            self.data = self.Close
            self.daily_data = self.d_close
        elif self.price_type == 'Open':
            self.data = self.Open
            self.daily_data = self.d_open
        elif self.price_type == 'Low':
            self.data = self.Low
            self.daily_data = self.d_low
        elif self.price_type == 'High':
            self.data = self.High
            self.daily_data = self.d_high


class samtalib:
    #thr_cr_perc : This is the thrshold for "Average Percentage of times the threshold was crossed".
    def __init__(self, price_type = 'Close', rsi_max = 57, rsi_period = 14, thr_cr_perc = 33, min_prof = 5, adx_thres = 25, adx_peroid = 14):
        self.tickers = {}
        self.list = []
        self.rsi_period = rsi_period
        self.price_type = price_type
        self.rsi_max = rsi_max
        self.min_prof = min_prof
        self.thr_cr_perc = thr_cr_perc
        self.adx_threshold = adx_thres
        self.adx_period = adx_peroid
        self.daily_info = {}

    def add_ticker_info(self, ticker, Open, High, Low, Close, Volume, d_open, d_high, d_low, d_close):
        if ticker in self.tickers:
            self.re_populate_data(ticker, Open, High, Low, Close, Volume, d_open, d_high, d_low, d_close)
            self.populate_macd(self.tickers[ticker])
            return
        stock = stock_info(ticker, Open, High, Low, Close, Volume, d_open, d_high, d_low, d_close)
        self.tickers[ticker] = stock
        self.list.append(ticker)
        self.populate_macd(stock)


    def populate_macd(self, stock):
        if stock.data is not None:
            stock.macd.macd, stock.macd.signal, stock.macd.hist = tb.MACD(stock.data, fastperiod = stock.macd.fastperiod, slowperiod = stock.macd.slowperiod, signalperiod = stock.macd.signalperiod)
            stock.adx = tb.ADX(stock.High, stock.Low, stock.Close, timeperiod = self.adx_period)
            stock.rsi = tb.RSI(stock.data, self.rsi_period)
            stock.plus_di = tb.PLUS_DI(stock.High, stock.Low, stock.Close, timeperiod = self.adx_period)
            stock.minus_di = tb.MINUS_DI(stock.High, stock.Low, stock.Close, timeperiod = self.adx_period)
        if stock.daily_data is not None:
            stock.daily_macd.macd, stock.daily_macd.signal, stock.daily_macd.hist = tb.MACD(stock.daily_data, fastperiod = stock.daily_macd.fastperiod, slowperiod = stock.daily_macd.slowperiod, signalperiod = stock.daily_macd.signalperiod)


    def re_populate_data(self, ticker, Open, High, Low, Close, Volume, d_open, d_high, d_low, d_close):
        stock = self.tickers[ticker]
        if Open is not None and len(Open) != len(stock.Open):
            stock.mail_added = 0
            for i in range(len(Open)):
                if str(stock.Open.index[-1]) < str(Open.index[i]):
                    stock.Open = stock.Open.append(Open[i:])
                    stock.High = stock.High.append(High[i:])
                    stock.Low = stock.Low.append(Low[i:])
                    stock.Close = stock.Close.append(Close[i:])
                    stock.Volume = stock.Volume.append(Volume[i:])
                    stock.setup_data()
                    break
        if stock.d_open is None:
            if d_open is not None:
                stock.d_open = d_open
                stock.d_high = d_high
                stock.d_low = d_low
                stock.d_close = d_close
                stock.setup_data()
            return
        if d_open is not None and len(d_open) != len(stock.d_open):
            for i in range(len(d_open)):
                if str(stock.d_open.index[-1]) < str(d_open.index[i]):
                    stock.d_open = stock.d_open.append(d_open[i:])
                    stock.d_high = stock.d_high.append(d_high[i:])
                    stock.d_low = stock.d_low.append(d_low[i:])
                    stock.d_close = stock.d_close.append(d_close[i:])
                    stock.setup_data()
                    break
        

    def get_macd(self, ticker, data):
        stock = self.tickers[ticker]
        stock.macd, stock.signal, stock.hist = tb.MACD(stock.data, stock.macd.fastperiod, stock.macd.slowperiod, stock.macd.signalperiod)
        
    def rsi_get(self, ticker, data, timeperiod):
        stock = self.tickers[ticker]
        stock.rsi = tb.RSI(stock.data, timeperiod)

    def is_buy(self, ticker):
        stock = self.tickers[ticker]
        if stock.macd > stock.signal and stock.rsi > stock.rsi_max:
            return True
        return False

    def is_sell(self, ticker):
        stock = self.tickers[ticker]
        if stock.macd < stock.signal and stock.rsi < stock.rsi_max:
            return True
        return False

    def check_if_crosssing(self, index, stock):
        if stock.macd.macd is None or len(stock.macd.macd) <= index:
            if stock.macd.macd is not None:
                del stock.macd.macd, stock.macd.signal, stock.macd.hist
            stock.macd.macd, stock.macd.signal, stock.macd.hist = tb.MACD(stock.data, fastperiod = stock.macd.fastperiod, slowperiod = stock.macd.slowperiod, signalperiod = stock.macd.signalperiod)

        if stock.macd.hist[index-1] < 0 and stock.macd.hist[index] > 0:
            return MACD_CROSSING_UPWARDS
        elif stock.macd.hist[index-1] > 0 and stock.macd.hist[index] < 0:
            return MACD_CROSSING_DOWNWARDS
        elif stock.macd.hist[index] > 0:
            return MACD_CROSSED_UP_ALREADY
        elif stock.macd.hist[index] < 0:
            return MACD_CROSSED_DOWN_ALREADY
        else:
            return MACD_CROSSING_NOTHING

    def get_index_for_daily_data(self, stock, date_str):
        if date_str > stock.daily_macd.hist.index[-1]:
            return len(stock.daily_macd.hist) - 1
        else:
            if date_str in stock.daily_data.index.tolist():
                return stock.daily_data.index.tolist().index(date_str) - 1
        #date_str not in the daily_data
        return -1

    # Check if the daily data macd is increasing.
    def check_if_daily_is_growing(self, stock, date_str):
        index = self.get_index_for_daily_data(stock, date_str)
        if index == -1:
            return False
        if math.isnan(stock.daily_macd.hist[index]) or math.isnan(stock.daily_macd.hist[index-1]) or math.isnan(stock.daily_macd.hist[index-2]):
            return False
        if stock.daily_macd.hist[index] > stock.daily_macd.hist[index - 1] and stock.daily_macd.hist[index - 1] > stock.daily_macd.hist[index - 2]:
            return True

    def get_next_crossing_index(self, stock, index, type_cross):
        for i in range(index + 1, len(stock.data)):
            if self.check_if_crosssing(i, stock) == type_cross:
                return i
        return len(stock.data) - 1

    def check_if_macd_caused_price_hike(self, stock, from_index, min_samples = 3):
        data = stock.data
        to_index = self.get_next_crossing_index(stock, from_index, MACD_CROSSING_DOWNWARDS)
        if to_index == len(data) or (to_index - from_index) < min_samples:
            stock.bullstics.neg_samples += 1
            return to_index
        from_index += 1
        max_price = data[from_index]
        min_price = data[from_index]
        for i in range(from_index + 1, to_index):
            if stock.High[i] > max_price:
                max_price = stock.High[i]
            elif stock.High[i] < min_price:
                min_price = stock.High[i]
        stock.bullstics.up_crossings.append([from_index, to_index, to_index - from_index])
        stock.bullstics.total_profit += max_price - data[from_index]
        stock.bullstics.total_loss += data[from_index] - min_price

        profit_perc = ((max_price - data[from_index])/data[from_index])*100
        stock.bullstics.avg_profit_perc += profit_perc
        if profit_perc >= self.min_prof:
            stock.bullstics.num_more_than_threshold += 1

        stock.bullstics.avg_loss_perc += ((data[from_index] - min_price)/data[from_index])*100
        stock.bullstics.num_samples += 1
        stock.bullstics.up_cross_max_min.append([(data[from_index] - min_price), max_price - data[from_index]])
        return to_index

    def crossing_take_action(self, stock, crossing, rsi, adx, plus_di, minus_di):
        if stock.init_done == 0 or stock.mail_added == 1:
            return
        
        if (crossing == MACD_CROSSING_UPWARDS or crossing == MACD_CROSSED_UP_ALREADY) and rsi >= self.rsi_max \
            and self.is_adx_indicating_uptrend(adx, plus_di, minus_di) == True:
            stock.mail_added = 1

        if stock.mail_added == 1:
            if stock.mail_str != "":
                del stock.mail_str
                stock.mail_str = ""
            self.string_add_stock_info(stock)
        
    def is_adx_indicating_uptrend(self, adx, plus_di, minux_di):
        if plus_di[-1] > minux_di[-1] and adx[-1] > self.adx_threshold:
            return True
        return False

    def fill_historic_data(self, stock, bull_threshold = 5):
        index_to_skip = 0
        bullstics = stock.bullstics
        if stock.prev_index == 0:
            from_range = 34 + 1
        else:
            from_range = stock.prev_index
        for i in range(from_range, len(stock.data)):
            if i < index_to_skip:
                continue
            crossing = self.check_if_crosssing(i, stock)
            if crossing == MACD_CROSSING_NOTHING or self.check_if_daily_is_growing(stock, stock.data._index[i].split()[0] + ' 00:00:00') == False:
                continue
            rsi = stock.rsi[i]
            adx = stock.adx[0:i]
            plus_di = stock.plus_di[0:i]
            minus_di = stock.minus_di[0:i]

            if (crossing == MACD_CROSSING_UPWARDS or crossing == MACD_CROSSED_UP_ALREADY) and rsi >= self.rsi_max \
                    and self.is_adx_indicating_uptrend(adx, plus_di, minus_di) == True:
                index_to_skip = self.check_if_macd_caused_price_hike(stock, i, bull_threshold)
                stock.last_noted_time = str(stock.High.index[i-1])

            self.crossing_take_action(stock, crossing, rsi, adx, plus_di, minus_di)
            del adx, plus_di, minus_di 

        stock.prev_index = len(stock.data)
        if bullstics.num_samples == 0:
            return
        bullstics.avg_num_profit_cross_threshold = (bullstics.num_more_than_threshold/(bullstics.num_samples + bullstics.neg_samples))*100
        stock.init_done = 1
        
    def string_add_stock_info(self, stock):
        if stock.mail_added == 0:
            if stock.mail_str != "":
                del stock.mail_str
                stock.mail_str = ""
            return

        bullstics = stock.bullstics
        if bullstics.avg_num_profit_cross_threshold < self.thr_cr_perc:
            return
        avg_profit_perc = (bullstics.avg_profit_perc/bullstics.num_samples)
        avg_loss_perc = (bullstics.avg_loss_perc/bullstics.num_samples)

        mail_str = ""
        mail_str += "<b>======================= %s ===================%s </b><br>"%(stock.ticker, stock.mail_message)
        profit = 0
        loss = 0
        mail_str += "<b>History of Similar events</b><br>"
        for i in range(len(bullstics.up_crossings)):
            from_index = bullstics.up_crossings[i][0]
            to_index = bullstics.up_crossings[i][1]
            mail_str += "From %s to %s, Price Increase %f , Price Decrease %f <br>"%(str(stock.Open.index[from_index + 1]), str(stock.Open.index[to_index]), bullstics.up_cross_max_min[i][1], bullstics.up_cross_max_min[i][0])
            profit += bullstics.up_cross_max_min[i][1]
            loss += bullstics.up_cross_max_min[i][0]

        mail_str += "<br><b>Analytics: </b><br>"
        mail_str += "Number of negative short impulses %d <br>"%bullstics.neg_samples
        mail_str += "Current Price: %f<br>"%stock.Close[-1]
        mail_str += "Analysis: Total Profit = %f, Total Loss = %f<br>"%(bullstics.total_profit, bullstics.total_loss)
        mail_str += "Net Profit = %f<br>"%(bullstics.total_profit - bullstics.total_loss)
        mail_str += "Average Percentage Profit: %f<br>"%avg_profit_perc
        mail_str += "Average Percentage Loss: %f<br>"%avg_loss_perc
        mail_str += "Last Noted Time when event Happened: %s<br>"%stock.last_noted_time
        mail_str += "<b>Average Percentage of times the threshold was crossed :%d</b><br><br>"%bullstics.avg_num_profit_cross_threshold

        stock.mail_str = mail_str
