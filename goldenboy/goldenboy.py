#!/bin/python3
import threading
import math
import datetime
from get_all_tickers import get_tickers as gt
import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
from stockbaba_lib import get_stock_info_lib
import traceback
import pdb
import json


class stock_threads(threading.Thread):
    def __init__(self, threadID, name, stock_list, from_index, to_index, arguements):
        threading.Thread.__init__(self)
        self.name = name
        self.stock_list = stock_list
        self.from_ind = int(from_index)
        self.to_ind = int(to_index)
        self.golden_stocks = []
        self.num_golden_stocks = 0
        self.input_args = arguements

    def run(self):
        for i in range(self.from_ind, self.to_ind):
            print("Downloading info of %s Thread name %s"%(list_of_tickers[i], self.name))
            sys.stdout.flush()
            stock = get_stock_info_lib.stock_basic_analysis(stock_ticker = list_of_tickers[i], start_d = self.input_args['goldenboy_filters']['from_date'], end_d = self.input_args['goldenboy_filters']['to_date'], index = i)
            if stock == None:
                print("Could not find stock %s"%list_of_tickers[i])
                continue
            valid = stock.goldenboy_filter_condition(min_price = self.input_args['goldenboy_filters']['min_price'], y_rise_thresold = self.input_args['goldenboy_filters']['yearly_rise'], c_drop_thresold = self.input_args['goldenboy_filters']['covid_drop'], num_div_thresold = self.input_args['goldenboy_filters']['minimum_num_dividends'], num_splits_threshold = self.input_args['goldenboy_filters']['minimum_stock_splits'])
            if valid == True:
                self.golden_stocks.append(stock)
            else:
                del stock
        self.num_golden_stocks = len(self.golden_stocks)



with open('./input.json') as input_file:
  input_args = json.load(input_file)

golden_stocks = []
list_of_tickers = gt.get_tickers()
#Currently the yf.download class is not Multithreading feasible.
total_num_stocks = len(list_of_tickers)
threadlist = []
start_index = 0
max_tickers_per_thread = 100000
for i in range(total_num_stocks//max_tickers_per_thread + 1):
    start_index = i*max_tickers_per_thread
    if total_num_stocks < (i+1)*max_tickers_per_thread:
        end_index = total_num_stocks
    else:
        end_index = (i+1)*max_tickers_per_thread

    thread_name = "Hunter-%d"%(i)
    thread_c = stock_threads(i, thread_name, list_of_tickers, from_index = start_index, to_index = end_index, arguements = input_args)
    threadlist.append(thread_c)
    if end_index == total_num_stocks:
        break

for i in range(len(threadlist)):
    threadlist[i].start()

for i in range(len(threadlist)):
    threadlist[i].join()
    golden_stocks = golden_stocks + threadlist[i].golden_stocks


print("There are %d stocks filtered." %(len(golden_stocks)))
for i in range(len(golden_stocks)):
    golden_stocks[i].info_get_stock()
