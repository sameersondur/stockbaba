#!/bin/python3
import math
import datetime

class monthly_average:
#Monthly Average Class
    def __init__(self):
        #Monthly Average Units. If the input is for 2 years. There will be 2 objects of monthly_average each having 12 entried for average_month, 
        #average_year and average_value.
        #Ex: from Jan 2019 to Dec 2020, will have:
        #   yearly_average.month_average[0].average_month[0] = '01', 
        #   yearly_average.month_average[0].average_year[0] = '2019', 
        #   yearly_average.month_average[0].average_value[0] = 100

        #   yearly_average.month_average[1].average_month[11] = '12', 
        #   yearly_average.month_average[1].average_year[23] = '2020', 
        #   yearly_average.month_average[1].average_value[0] = 200
        self.average_month = []
        self.average_year = []
        self.average_value = []

    def insert_val(self, month, year, val):
        self.average_month.append(month)
        self.average_year.append(year)
        self.average_value.append(val)

    #If I want to execute a lot of data sets on laptops, then I want to make sure that I try and release memeory.
    #Python Memory Manager does not free the memory immediately. It might wait till there are no refernces to the object.
    #I need to just try and get rid of the memory if possible.
    def __del__(self):
        del self.average_month[:]
        del self.average_year[:]
        del self.average_value[:]

class yearly_average(monthly_average):
    #Yearly Average Units. Ex: from Jan 2019 to Dec 2020.
    def __init__(self):
        self.average_year = []
        self.average_value = []
        self.month_average = []

    def insert_val(self, year, val, month_average):
        self.average_year.append(year)
        self.average_value.append(val)
        self.month_average.append(month_average)

    def calculate_average(self, years, months, days, values):
        year_index = 0
        year_average = float(0)
        year = years[0]
        month = months[0]
        m_average = float(0)
        month_average = monthly_average()
        monthly_days = 0
        yearly_days = 0
        total_len = len(values)
        
        for i in range(total_len):
            if year != years[i]:
                month_average.insert_val(month = month, year = year, val = m_average/monthly_days)
                self.insert_val(year = year, val = year_average/yearly_days, month_average = month_average)
                year = years[i]
                month = months[i]
                monthly_days = 0
                yearly_days = 0
                year_average = float(0)
                m_average = float(0)
                month_average = monthly_average()
            if month != months[i]:
                month_average.insert_val(month = month, year = year, val = m_average/monthly_days)
                month = months[i]
                monthly_days = 0
                m_average = float(0)
            monthly_days += 1
            yearly_days += 1
            m_average = m_average + values[i]
            year_average = year_average + values[i]
            if i == (total_len-1):
                month_average.insert_val(month = month, year = year, val = m_average/monthly_days)
                self.insert_val(year = year, val = year_average/yearly_days,  month_average = month_average)

    #If I want to execute a lot of data sets on laptops, then I want to make sure that I try and release memeory.
    #Python Memory Manager does not free the memory immediately. It might wait till there are no refernces to the object.
    #I need to just try and get rid of the memory if possible.
    def __del__(self):
        del self.average_year[:]
        del self.average_value[:]
        del self.month_average[:]

class date_info(yearly_average):
    def __init__(self):
        self.date = []
        self.year = []
        self.month = []
        self.day = []
        self.time = []
        self.y_avg = yearly_average()

    def parse_date(self, data):
        self.date.append(data.split()[0])
        self.time.append(data.split()[1])
        self.year.append(self.date[-1].split('-')[0])
        self.month.append(self.date[-1].split('-')[1])
        self.day.append(self.date[-1].split('-')[2])

    def date_average(self, val):
        self.y_avg.calculate_average(years = self.year, months = self.month, days = self.day, values = val)

    def date_print_averages(self):
        for i in range(len(self.y_avg.average_year)):
            print("=====Year %s  Average $%d =====" %(self.y_avg.average_year[i], self.y_avg.average_value[i]))
            print('{:<20s}{:<20s}'.format("Average","Month"))
            for j in range(len(self.y_avg.month_average[i].average_month)):
                buff = "%s-%s"%(self.y_avg.month_average[i].average_month[j], self.y_avg.month_average[i].average_year[j])
                print('{:<20s}{:<20f}'.format(buff, self.y_avg.month_average[i].average_value[j]))

    #Get the array of montly averages for a year
    def get_montly_averages(self, year):
        if isinstance(year, str) == False:
            year = str(year)

        for i in range(len(self.y_avg.average_year)):
            if (year == self.y_avg.average_year[i]):
                return self.y_avg.month_average[i].average_value
        return None


    #Get the average for the entire year.
    def get_year_average(self, year):
        if isinstance(year, str) == False:
            year = str(year)

        for i in range(len(self.y_avg.average_year)):
            if (year == self.y_avg.average_year[i]):
                return self.y_avg.average_value[i]

        return None

    #Get the array of all the averages
    def get_yearly_averages(self):
        return self.y_avg.average_value

    #Get the list of all years
    def get_year_list(self):
        return self.y_avg.average_year

    #If I want to execute a lot of data sets on laptops, then I want to make sure that I try and release memeory.
    #Python Memory Manager does not free the memory immediately. It might wait till there are no refernces to the object.
    #I need to just try and get rid of the memory if possible.
    def __del__(self):
        del self.date[:]
        del self.year[:]
        del self.month[:]
        del self.day[:]
        del self.time[:]
        del self.y_avg
