# Author: Sameer Ashok Sondur

# What do I want to achieve?
Due to COVID-19. Many of the stocks have gone Bear. I need to Identify highest Potential Stocks, that will yield a long term Gain by 2023.
I am assuming that by 2023 the COVID-19 situation will become normal and the life will be back to where it was in Nov-2019. 
So, I need to identify stocks that might survive the storm but are now available at a very cheap price and then return to their normal price by 2023.

# How do I get the a list of stocks which have minimum criteria?
1. Get the list of all Tickers.
2. Consider 1 Ticker.
3. Find the monthly and yearly average price of the stock in past 4 years. ex: from_date = 2017-01-01,  to_date = 2020-11-18
4. See if the average price of stock was increasing in past 3 years and has dropped in 2020. If yes then consider it further. ex: covid_drop = 10%
5. Setup a threshold for % of increase of stock price from 2017 to 2019. ex: yearly_rise = 5%
6. Filter all the stocks that pass these filters.
7. Filter all the stocks that have announced dividends before for more than a threashold. ex: minimum_num_dividends = 0
8. Also, tag the stocks that have undergone a forward split for more than threshold times.
9. Filter the stocks that have undergone a forward split more than or equal to threshold number of times. ex: minimum_stock_splits = 1
10. Filter out the stocks that have not recovered. Ex: If a current stock price has gone more than the previous year's average, then drop it.
11. You will now get a list of goldenboys that have the ability to do well.

# After I get the list of all the above stocks, how do I choose from it?
1. Go to tradingview and verify the above filters are correct for the stock.
2. Check their earnings.
3. If earnings less than 50M, then the risk of bankruptcy is high.
4. Check if there is some news that might indicate the company's future.
5. Do Fundamental and Technical Analysis.

