#!/bin/python3
from urllib.request import urlopen, Request
from bs4 import BeautifulSoup
import os
import sys
import pandas as pd
from fake_useragent import UserAgent
import traceback
import re
from urllib.error import HTTPError

class web_scraper():
    def __init__(self, url, date_t, time_t, title, response = None, debug = False):
        self.text = ""
        self.url = url
        self.debug = debug
        self.date = date_t
        self.time = time_t
        self.title = title
        self.response = response

    def __del__(self):
        del self.text
        del self.url
        del self.debug
        del self.date
        del self.time
        del self.title

    def exception_happened(self):
        print("Shit happened here at below URL")
        print(self.url)
        traceback.print_exc()

    def handle_text(self, text):
        text = re.sub("\n","", text)
        text = re.sub("\t"," ", text)
        return re.sub(' +', ' ', text)

    def get_url_to_html(self):
        if self.debug == True:
            print("URL: %s"%self.url)
        if self.response == None:
            req = Request(url=self.url, headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'})
            try:
                response = urlopen(req)
            except HTTPError as err:
                if err.code == 404:
                    print("This URL was not found: %s"%self.url)
                return None
        
        html = BeautifulSoup(self.response, "lxml")
        return html

    def get_fooldotcom_info(self):
        if "fool.com/investing" not in self.url and "fool.com/earnings" not in self.url:
            return None
        try:
            html = self.get_url_to_html()
            if html == None:
                return
            header = html.find("section", class_="usmf-new article-header")
            article_page = html.find("span", class_="article-content")
            self.html = article_page
            all_paragraphs = article_page.findAll('p')
            self.text = header.h1.text
            for element in all_paragraphs:
                self.text = self.text + '\n' + self.handle_text(text = element.text)

        except Exception as e:
            print(e)
            self.exception_happened()
    
    def get_yfinance_info(self):
        if "finance.yahoo" not in self.url:
            return None
        try:
            html = self.get_url_to_html()
            if html == None:
                return
            header = html.find("div", class_="caas-title-wrapper")
            article_page = html.find("div", {"class": "caas-body-content"})
            article_int = article_page.find("div", {"class": "caas-body"})
            self.html = article_int
            all_paragraphs = article_int.findAll('p')
            self.text = header.h1.text
            for element in all_paragraphs:
                if "Related Content" in element.text:
                    break
                if "Disclosure:" in element.text:
                    break
                self.text = self.text + '\n' + self.handle_text(text = element.text)

        except Exception as e:
            print(e)
            self.exception_happened()
    
    def get_barrons_info(self):
        if "barrons.com" not in self.url:
            return None

        try:
            html = self.get_url_to_html()
            if html == None:
                return
            article_body= html.find(id = "article-contents")
            self.html = article_body
            all_paragraphs = article_body.findAll('p')
            for element in all_paragraphs:
                self.text = self.text + '\n' + self.handle_text(text = element.text)

        except Exception as e:
            print(e)
            self.exception_happened()
    
    def get_relamoney_info(self):
        if "realmoney.thestreet.com" not in self.url:
            return None
        try:
            html = self.get_url_to_html()
            if html == None:
                return
            article_body= html.find("div", {"class": "article__body"})
            self.html = article_body
            all_paragraphs = article_body.findAll('p')
            all_divs = article_body.findAll('div')
            for element in all_paragraphs:
                self.text = self.text + '\n' + self.handle_text(text = element.text)
            for element in all_divs:
                self.text = self.text + '\n' + self.handle_text(text = element.text)
        except Exception as e:
            print(e)
            self.exception_happened()


    def get_thestreet_info(self):
        if "www.thestreet.com" not in self.url:
            return None

        try:
            html = self.get_url_to_html()
            if html == None:
                return
            article_body= html.find("div", {"class": "m-detail--body"})
            self.html = article_body
            all_paragraphs = article_body.findAll('p')
            for element in all_paragraphs:
                self.text = self.text + '\n' + self.handle_text(text = element.text)

        except Exception as e:
            print(e)
            self.exception_happened()
    

    def get_investopedia_info(self):
        if "www.investopedia.com" not in self.url:
            return None

        try:
            html = self.get_url_to_html()
            if html == None:
                return
            article_body= html.find(id="mntl-sc-page_1-0")
            self.html = article_body
            all_paragraphs = article_body.findAll('p')
            for element in all_paragraphs:
                self.text = self.text + '\n' + self.handle_text(text = element.text)

        except Exception as e:
            print(e)
            self.exception_happened()

    def get_marketwatch_info(self):
        if "www.marketwatch.com/story" not in self.url:
            return None

        try:
            html = self.get_url_to_html()
            if html == None:
                return
            article_body = html.find(id = "js-article__body")
            article_int = article_body.find("div", {"class": "paywall"})
            self.html = article_int
            all_paragraphs = article_int.findAll('p')
            for element in all_paragraphs:
                text = element.text
                self.text = self.text + '\n' + self.handle_text(text = text)

        except Exception as e:
            print(e)
            self.exception_happened()

    def get_investors_com_info(self):
        if "www.investors.com" not in self.url:
            return None

        try:
            html = self.get_url_to_html()
            if html == None:
                return
            article_body = html.find("section", class_="stock-market-today")
            article_int = article_body.find("div", {"class": "single-post-content"})
            self.html = article_int
            all_paragraphs = article_int.findAll('p')
            for element in all_paragraphs:
                text = element.text
                self.text = self.text + '\n' + self.handle_text(text = text)

        except Exception as e:
            print(e)
            self.exception_happened()
 
    def get_article_text_from_url(self):
        if "fool.com/investing" in self.url or "fool.com/earnings" in self.url:
            self.get_fooldotcom_info()
        elif "finance.yahoo" in self.url:
            self.get_yfinance_info()
        elif "barrons.com" in self.url:
            self.get_barrons_info()
        elif "thestreet.com" in self.url:
            self.get_thestreet_info()
        elif "www.investopedia.com" in self.url:
            self.get_investopedia_info()
        elif "www.marketwatch.com/story" in self.url:
            self.get_marketwatch_info()
        elif "www.investors.com" in self.url:
            self.get_investors_com_info()
        elif "realmoney.thestreet.com" in self.url:
            self.get_relamoney_info()
        else:
            print("Url unidentified")
            print(self.url)
            sys.exit(0)
