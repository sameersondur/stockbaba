#!/bin/python3
from flask import Flask, render_template, render_template_string, Response, request, redirect
import os, sys, json
import subprocess
import shutil

app = Flask('SamCorder', root_path='/home/sameer/stocks/samstocks/webserver')
html_gen = None
settings = {}

def load_settings():
    global settings
    if os.path.exists("settings/main.json") == False:
        print("Main settings file does not exist")
        sys.exit(1)
    f = open ("settings/main.json", "r")
    settings = json.loads(f.read())
    f.close()

def get_basic_header(header_title = "Sam's Corner"):
    with open('templates/basic_header.html', 'r') as file:
        header_data = file.read()
    if header_data == None:
        print("templates/basic_header.html does not exist")
        return ""
    header_data = header_data.replace("TITLE_SAM_DO_IT", header_title)
    return header_data

def sam_html_gen():
    data = None
    html_data = get_basic_header("List of Objects")
    
    with open('data.json') as json_file:
        data = json.load(json_file)
    if data == None:
        html_data = html_data + "<body><h1>Failed to Open the file</h1></body></html>"
        with open("./templates/list_objects.html", "w") as text_file:
            text_file.write(html_data)
        return
    
    html_data += """<div style="overflow-x:auto;">\n    <table><tr><th>Ticker</th><th>Name of Company</th></tr>"""
    for ele in data['bullrider']:
        html_data += "      <tr><td>" + ele["ticker"] + "</td><td>" + ele["company_name"] + "</td></tr>\n"
    html_data += "</table>\n</div>\n</html>"
    with open("./templates/list_objects.html", "w") as text_file:
        text_file.write(html_data)
    return

def find_replace_news_html(data):
    filename = "templates/news_data.html"
    if os.path.exists(filename) == True:
        os.remove(filename)
    shutil.copyfile("templates/news_data_template.html", filename)

    with open(filename, 'r') as f:
        filedata = f.read()

    with open(filename, 'w') as f:
        f.write(filedata)


@app.route('/')
def hello():
    return render_template('homepage.html')

@app.route('/js/<name>')
def render_js(name):
    if os.path.exists("templates/js/"+name) == False:
        return render_template('404.html'), 404
    with open("templates/js/"+name, 'r') as f:
        return f.read()

@app.route('/assets/<name>')
def render_assets(name):
    if os.path.exists("templates/assets/"+name) == False:
        return render_template('404.html'), 404
    with open("templates/assets/"+name, 'r') as f:
        resp = Response(f.read())
        resp.headers['Content-Type'] = 'image/svg+xml'
        return resp

@app.route('/tradviewlink')
def tradingview():
    global settings
    load_settings()
    base_url = settings['tradingview']
    args = request.args
    ticker = args.get('query')
    return redirect(base_url+ticker)

@app.route('/output_news')
def output_json():
    with open("./output/stock_news.json", 'r') as f:
        return f.read()

@app.route('/news')
def news_spawn():
    global settings
    load_settings()
    args = request.args
    ticker = args.get('query')
    if os.path.isdir("output") == False:
        mode = 0o666
        os.mkdir('output', mode)
    cmd = "../sentiquity/sentiquity_lean.py --ticker "+ticker+" --max_urls "+ str(settings['max_urls'])
    ret = subprocess.run(cmd, shell=True)
    if ret.returncode != 0:
        return render_template('Sentiquity failed!!'), 404
    out_path = "./output/stock_news.json"
    if os.path.exists(out_path) == False:
        return render_template('404.html'), 404
    with open("./output/stock_news.json", 'r') as f:
        data = json.load(f)

    find_replace_news_html(data)
    return render_template('news_data.html')

@app.route('/testing')
def testing():
    return render_template('testing.html')
if __name__ == '__main__':
    load_settings()
    app.run(host = '10.0.2.15')
