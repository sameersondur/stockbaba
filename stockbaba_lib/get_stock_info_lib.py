#!/bin/python3
import yfinance as yf
import argparse
import math
import datetime
from get_all_tickers import get_tickers as gt
import os, sys
from stockbaba_lib import average_lib
import traceback
import pdb


class stock_basic_analysis(average_lib.date_info):
    def difference(self, val_a, val_b):
        if val_a > val_b:
            return float(val_a - val_b)
        return float(val_b - val_a)

    def block_stdout(self):
        f = open(os.devnull, 'w')
        self.out = sys.stdout
        sys.stdout = f

    def unblock_stdout(self):
        f = sys.stdout
        if not hasattr(self, 'out'):
            return
        sys.stdout = self.out
        f.close()
        del self.out

    def __init__(self, stock_ticker, index, start_d = "", end_d = "", auto_adjust = False, interval_a = None):
        self.valid_entries = 0
        self.average_total = float(0)
        self.date_info = average_lib.date_info()
        self.ticker = stock_ticker
        self.start = start_d
        self.end = end_d
        self.average_price = []
        self.info = yf.Ticker(stock_ticker)
        self.index = index
        #yf.pdr_override()
        #Just Observed that if we supress the stdout here then we get the processing faster by 160%.
        #self.block_stdout()
        try:
            if interval_a:
                self.data = yf.download(self.ticker, start = start_d, end = end_d, Threads= False, auto_adjust = auto_adjust, interval = interval_a)
            else:
                self.data = yf.download(self.ticker, start = start_d, end = end_d, auto_adjust = auto_adjust, Threads= False)
        except Exception as inst:
            print(type(inst))
            print(inst.args) 
            print(inst) 
            return None
        self.unblock_stdout()
        #self.data = pdr.get_data_yahoo(self.ticker, start = start_d, end = end_d)
        if len(self.data.Open) == 0:
            return None
        for i in range(len(self.data.Open)):
            if math.isnan(self.data.Open[i]):
                continue
            self.date_info.parse_date(str(self.data.Open._index[i]))
            self.average_price.append((self.data.Open[i]+self.data.Close[i])/2)
            self.average_total = self.average_total + (self.data.Open[i]+self.data.Close[i])/2
            self.valid_entries = self.valid_entries + 1
        self.average_total = self.average_total/self.valid_entries
        self.monthly_yearly_average()
        self.covid_decrement = float(0)
        self.y_avg_rise = float(0)
        self.y_figures = {}
        self.y_figures['yearly_info'] = []
        self.y_figures['average'] = []
        try:
            self.company_name = self.info.info['longName']
        except:
            self.company_name = 'No Info'
        try:
            self.industry_name = self.info.info['industry']
        except:
            self.industry_name = 'No Info'
        try:
            self.sector_name = self.info.info['sector']
        except:
            self.sector_name = 'No Info'


    def monthly_yearly_average(self):
        self.date_info.date_average(self.average_price)

    def info_get_stock(self, detail = False):
        if detail == False:
            print("=====================================================")
            print("The Stock is : %s" %self.ticker)
            print("The Price Tanked by %f percent in COVID"%self.covid_decrement)
            y_average_value = self.date_info.get_yearly_averages()
            y_average_years = self.date_info.get_year_list()
            for i in range(len(y_average_value)):
                print("\tThe Average Price of Stock in %s was $%f"%(y_average_years[i], y_average_value[i]))
            print("The Stock has issued Dividends %d times."%(len(self.info.dividends)));
            print("THe Stock has Split %d times."%(len(self.info.splits)))
            print("The average Price between %s and %s is: $%d" %(self.start, self.end, self.average_total))
            return
        print("=====================================================")
        print("                    %s                      " %self.ticker)
        print("=====================================================")
        print("The Info Regarding the Stock: %s" %self.ticker)
        print("=====Dividends========")
        if len(self.info.dividends) == 0:
            print("N/A")
        else:
            print(self.info.dividends[-5:])
        print("=====Splits===========")
        if len(self.info.splits) == 0:
            print("N/A")
        else:
            print(self.info.splits[-5:])
        if len(self.start) > 0:
            print("The average Price between %s and %s is: $%d" %(self.start, self.end, self.average_total))
        if detail == True:
            print(self.data)
        self.date_info.date_print_averages()

    def stock_details_json_object(self):
        try:
            company_name = self.info.info['longName']
        except:
            company_name = 'No Info'
        try:
            industry_name = self.info.info['industry']
        except:
            industry_name = 'No Info'
        try:
            sector_name = self.info.info['sector']
        except:
            sector_name = 'No Info'

        json_data = {
            'ticker': self.ticker,
            'company_name': company_name,
            'sector': sector_name,
            'industry': industry_name,
            'num_dividends': len(self.info.dividends),
            'num_positive_splits': len(self.info.splits),
            'yearly_average': self.y_figures
        }
        return json_data

    # Check if the average stock price is rising before 2020 and it dropped after 2020
    # yearly_incr_threshold = Yearly % increase thresold
    # covid_decr_threshold = % of drop due to COVID
    def check_covid_if_yearly_rising(self, yearly_incr_threshold, covid_decr_threshold):
        prev = float(0)
        yearly_avgs = self.date_info.get_yearly_averages()
        year_list = self.date_info.get_year_list()
        for i in range(len(year_list)):
            year_avg = float(yearly_avgs[i])
            if year_list[i] == '2020':
                if i == 0:
                    #you have just 1 year and it is 2020
                    return False
                diff = year_avg - prev
                #This would mean that the price incresed after COVID
                if ((diff/prev *100) > 0):
                    return False
                #Check the % of price drop against covid_decr_threshold
                diff = diff*(-1)
                self.covid_decrement = diff/prev *100
                if ((self.covid_decrement) > covid_decr_threshold):
                    return True
                else:
                    return False
            if i == 0:
                prev = year_avg
                continue
            if year_avg < prev:
                return False
            else:
                diff = year_avg - prev
                #check if the growth w.r.t the previous year is les than threshold
                if ((diff/prev * 100) < yearly_incr_threshold):
                    return False
                prev = year_avg
        #If you are not considering 2020, then the loop will not be complete.
        return False


    # Check if the average stock price is rising consistantly 
    # yearly_incr_threshold = Yearly % increase thresold
    # return = List containig the True/False and the yearly rise average
    def check_if_yearly_rising(self, yearly_incr_threshold):
        prev = float(0)
        yearly_rise_average  = float(0)
        yearly_avgs = self.date_info.get_yearly_averages()
        year_list = self.date_info.get_year_list()
        num_yearly_entries = float(0)

        #If there is less than 3 years fo data, then reject it.
        if len(year_list) <= 2:
            return False 

        for i in range(len(year_list)):
            year_avg = float(yearly_avgs[i])
            if i == 0:
                prev = year_avg
                continue

            if year_avg < prev:
                return False
            else:
                diff = year_avg - prev
                #check if the growth w.r.t the previous year is less than threshold
                rise_val = (diff/prev * 100)
                if (rise_val < yearly_incr_threshold):
                    return False
                buff = "%s-%s"%(year_list[i-1], year_list[i])
                self.y_figures['yearly_info'].append({
                    'year_info' : buff,
                    'year_rise' : rise_val
                })
                yearly_rise_average += rise_val
                prev = year_avg
                num_yearly_entries += 1
        #If no failures, then return True
        self.y_figures['average'].append({
            'average_rise' : yearly_rise_average/num_yearly_entries
        })
        return True


    #num_div_thresold: number of dividend instances threshold.
    #num_splits_threshold: number of split instances thresold.
    #check_rev_split: Bool: Check if the stock split was reverse.
    def check_dividends_splits(self, num_div_thresold, num_splits_threshold, check_rev_split = True):
        try:
            if num_div_thresold != 0:
                if len(self.info.dividends) < num_div_thresold:
                    return False
            if check_rev_split == True:
                if len(self.info.splits) >= num_splits_threshold:
                    for i in range(len(self.info.splits)):
                        if float(self.info.splits[i] < 1):
                            return False
            if len(self.info.splits) < num_splits_threshold:
                return False
            return True
        #In some stocks like GCMGW and MNCLW for some reason the access to self.info.dividends and self.info.splits thows TypeError.
        #THe valkue of them is neither not None, but we cannot access them. Also, their values do not exist in real world.
        except TypeError:
            return False

    #If you do not want penny stocks or low priced stocks, then you use this API.
    #num_thresolds: If there is are more than num_thresolds years when the average stock price is less min_price, return False.
    def check_minimum_stock_price(self, min_price, num_thresolds = 0):
        threshold = int(0)
        yearly_avgs = self.date_info.get_yearly_averages()
        year_list = self.date_info.get_year_list()
        for i in range(len(year_list)):
            year_avg = float(yearly_avgs[i])
            if year_avg < min_price:
                threshold += 1
            if threshold > num_thresolds:
                return False
        return True

    #There are some stocks that had gone down for long time after March, but have already recovered.
    #If the Current price is more than the price of the previous year's average, then drop it.
    def check_sock_recoverd_after_covid(self):
        yearly_avgs = self.date_info.get_yearly_averages()
        year_list = self.date_info.get_year_list()
        year_2019_avg = float(0)
        for i in range(len(yearly_avgs)):
            if year_list[i] == '2019':
                year_2019_avg = yearly_avgs[i]
                break
        if year_2019_avg == 0:
            return False
        if float(self.data.Open[-1]) > year_2019_avg:
            return False
        return True

    #y_rise_thresold: Yearly % increase thresold
    #num_div_thresold: number of dividend instances threshold.
    #num_splits_threshold: number of split instances thresold.
    #check_rev_split: Bool: Check if the stock split was reverse.
    def goldenboy_filter_condition(self, min_price, y_rise_thresold, c_drop_thresold, num_div_thresold, num_splits_threshold, check_rev_split = True, num_thresolds = 0):
        if self.check_minimum_stock_price(min_price = min_price, num_thresolds = num_thresolds) == False:
            return False
        if self.check_covid_if_yearly_rising(yearly_incr_threshold = y_rise_thresold, covid_decr_threshold = c_drop_thresold) == False:
            return False
        if self.check_dividends_splits(num_div_thresold = num_div_thresold, num_splits_threshold = num_splits_threshold, check_rev_split = check_rev_split) == False:
            return False
        if self.check_sock_recoverd_after_covid() == False:
            return False
        return True

    def bullrider_filter_condition(self, min_price, y_rise_thresold, num_div_thresold, num_splits_threshold, check_rev_split = True, num_thresolds = 0):
        if self.check_minimum_stock_price(min_price = min_price, num_thresolds = num_thresolds) == False:
            return False
        if self.check_if_yearly_rising(yearly_incr_threshold = y_rise_thresold) == False:
            return False
        if self.check_dividends_splits(num_div_thresold = num_div_thresold, num_splits_threshold = num_splits_threshold, check_rev_split = check_rev_split) == False:
            return False
        return True

    def __del__(self):
        del self.valid_entries
        del self.average_total
        del self.date_info
        del self.ticker
        del self.start
        del self.end
        del self.average_price[:]
        del self.info
        del self.data


