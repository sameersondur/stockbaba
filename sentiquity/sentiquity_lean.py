#!/bin/python3
import threading
from multiprocessing import Process, Queue
import math
import datetime
import os, sys
import traceback
import pdb
import json
import time
import glob
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
from stockbaba_lib import dataframfile
sys.path.append(os.environ["POLYGON_LIB_DIR"])
import polylibs
from stockbaba_lib import stock_news
import argparse

def get_info(list_stocks, max_links):
    stock_news_list = []
    for ele in list_stocks:
        news = stock_news.stock_news(ele, max_links)
        news.print_json()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--ticker', type=str, required=True, action = 'append', nargs=1)
    parser.add_argument("--max_urls", type = int, required = False, default = 10)
    args = parser.parse_args()
    
    list_stocks = []
    for ele in args.ticker:
        list_stocks.append(ele[0])

    get_info(list_stocks, args.max_urls)
