# Author: Sameer Ashok Sondur

# What do I want to achieve?
Want to map the sentiment of all stocks based on the articles writtten about them.

# What are the steps to achieve this?
    To achieve ths task I am dividng this project into different parts.
## Web Scraping
This is the art of scraping the web to get the information you need.
1. Use finviz.com to get the list of all the links written regarding the stock. 
2. Why finviz? The web scraping depends on inspecting the html output of a website. finviz is free. Also, based on the research the UI of the website has not changed for a long time. So, the probability of it changing now is less.
3. Once you get the link of all the articles, you need to go to those links individually and find the text of the article by again scraping it.
4. Different websites might have different html format. So, for each website, you need to find the format and make a Look Up Table.
5. Store the output in a structured json format. The output directory contains the json files for each ticker.
6. Each ticker has a corresponding json file and each article in a ticker has a article text file.
  Ex: The below is the example of the json file
```json
    {
      "ticker": "AMZN",
      "company_name": "Amazon.com, Inc.",
      "sector": "Consumer Cyclical",
      "industry": "Internet Retail",
      "num_dividends": 0,
      "num_positive_splits": 3,
      "finviz_url": "https://finviz.com/quote.ashx?t=AMZN",
      "all_url_infos": [
        {
          "url": "https://www.fool.com/investing/2020/12/08/c3ai-ipo-what-investors-need-to-know/?source=eptyholnk0000202&utm_source=yahoo-host&utm_medium=feed&utm_campaign=article",
          "article_title": "C3.ai IPO: What Investors Need to Know",
          "article_text_file": "/root/stockmarket/stockbaba/sentiquity/output/stock_news_article_AMZN_0.txt",
          "date": "Dec-08-20",
          "time": "08:23PM",
          "sentiment": 0
        },
        {
          "url": "https://finance.yahoo.com/news/sleeping-giant-e-commerce-next-005712683.html",
          "article_title": "This Sleeping Giant of e-Commerce is the Next Amazon",
          "article_text_file": "/root/stockmarket/stockbaba/sentiquity/output/stock_news_article_AMZN_1.txt",
          "date": "Dec-08-20",
          "time": "07:57PM",
          "sentiment": 0
        },
        {
          "url": "https://www.marketwatch.com/story/why-amazon-was-invented-for-the-pandemic-and-how-aws-could-be-the-most-valuable-company-in-the-world-11607449670?siteid=yhoof2",
          "article_title": "Scott Galloway on why Amazon was invented for the pandemic, and how AWS could be the most valuable company in the world",
          "article_text_file": "/root/stockmarket/stockbaba/sentiquity/output/stock_news_article_AMZN_2.txt",
          "date": "Dec-08-20",
          "time": "07:19PM",
          "sentiment": 0
        },
        {
          "url": "https://finance.yahoo.com/video/premium-trading-fast-market-paradigm-234509465.html",
          "article_title": "PREMIUM: Trading the new 'fast market' paradigm",
          "article_text_file": "/root/stockmarket/stockbaba/sentiquity/output/stock_news_article_AMZN_3.txt",
          "date": "Dec-08-20",
          "time": "06:45PM",
          "sentiment": 0
        },
        {
          "url": "https://www.investors.com/news/disney-stock-streaming-video-investor-day-after-warner-bros-shocker/?src=A00220",
          "article_title": "All Eyes On Disney As Hollywood Reels From Warner Bros. Shocker",
          "article_text_file": "/root/stockmarket/stockbaba/sentiquity/output/stock_news_article_AMZN_4.txt",
          "date": "Dec-08-20",
          "time": "04:18PM",
          "sentiment": 0
        }
      ]
    }
```

## Email you the Articles on demand
We need to look at the articles regarding the stocks very regularly. I do not want to go through etrade with all the stocks in my portfolio and look at all the articles by analysts. I have more than 10 stocks in my portfolio.
What if the list of articles comes to you at the time set by you? What if you can request for this list anytime?
1. Fill out a google Form. This Form has the following details:
    * Your email id
    * List of tickers
    * Time at which you want the info to be mailed to you in PST everyday.
    * Unsubscribe if you want to.
    * Get the list of links now.
2. This Form details are filled into a google sheet.
3. A cron job is run every N minutes which checks the status of the sheet. If the sheet is changed, then the database info is changed.
4. Database has the info regarding the person, his tickers and the deadline to dispatch the articles.
5. Every N minutes, the deadline is checked and if the deadline has crossed or reached, the articles are sent to the email address.

## Sentiment Analysis
This is done with the help of python libraries, that will give you the sentiment of a text.
Need to find out a way to analyse the text if it talks about more than 1 stock.
