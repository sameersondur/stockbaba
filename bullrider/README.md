# Author: Sameer Ashok Sondur

# What do I want to achieve?
I want a tool which will give me the list of stocks that have a constant rise of atleast X% per annum for past N years.

# How do I get the a list of stocks which have minimum criteria?
1. Get the list of all Tickers.
2. yfinance is not multithreading friendly. So, it is best if we fork the job into multiple processes.
3. You would want to limit the number of stocks to be processed per forked job. E: If there are 7000 stocks and min_stocks_per_fork = 500. With this, the number of stocks that will be processed per forked unit will be 500 and there will be 14 forked jobs running.
4. In a forked process. Consider 1 Ticker.
5. Find the monthly and yearly average price of the stock in past N years. ex: from_date = 2017-01-01,  to_date = 2020-11-18
6. See if the average price of stock was increasing in past N years. Ex: least_rise = 5
7. Output the values of each forked process to a json file.
8. After joining all the threads with forks, you need to sort all the stocks based on the % of rise.

