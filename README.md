# What do I want to Achieve?
I recently came accross stock market and I thought that let me try to automate some stuff. 
I am pretty sure that there are many other tools in market that are more efficient. This is just for Fun. Do not consider this to be a tool that will make you shit load of money and make you a millionare. 
If you do become a millionare with this then please consider donating to a nearby orphanage. Also, let me know how you used tools :).


# Disclaimer to PROSPECTIVE EMPLOYERS:
Please do not think that I know a lot of python/pine-script. I request you to not ask me python related quesitons in interview :) . I am an Embedded Engineer.

# List of Projects
## Project Goldenboy: Post COVID golden stock.
This will give you the list of stocks that were doing good before COVID and flunked after COVID. But have a potential for long term investment.
Checkout directory **goldenboy**

## Project Bullrider: Get the stock which is rising since last N years
This will give you the list of stocks that are rising since the last N years. The minimum rate of rise is provided by the user.
Checkout directory **bullrider**

## Project Sentiquity: Get the sentiment of a stock in the market based on articles
Sentiquity means Sentiment of an Equity. There are many articles written regarding different stocks daily.
What if there is a way to get the sentiment of every stock that has something written about it?
