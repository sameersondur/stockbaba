A Trio of High Return on Equity Ratio Stock Picks to Consider
- By Alberto Abaterusso
When a company's return on equity (ROE) ratio beats most of its competitors, it implies the company has been very efficient in generating earnings.
Thus, investors may want to have a look at the following companies, as they are topping most of their competitors in terms of a higher ROE ratio.
RELX PLC 
List of 52-Week Lows
List of 3-Year Lows
List of 5-Year Lows

The first stock is RELX PLC (NYSE:RELX), an English provider of information-based analytics that support various organizations to make important advancements in their activities, enhance the decision-making process and increase productivity.
RELX PLC has a ROE ratio of 59.3%, while the industry median has a ROE ratio of -1.78%.
The share price growth was flat over the past year and closed at $23.64 on Wednesday for a market capitalization of $45.68 billion and a 52-week range of $16.81 to $27.26.
The stock has a price-book ratio of 16.37 (versus the industry median of 1.57) and a price-earnings ratio of 26.95 (versus the industry median of 22.5).
GuruFocus has assigned a financial strength rating of 4 out of 10 and a profitability rating of 8 out of 10 to the company.
On Wall Street, the stock has an overweight recommendation rating with an average target price of $28.
Deckers Outdoor Corp 
The second stock to consider is Deckers Outdoor Corp (NYSE:DECK), a Goleta, California-based designer and distributor of casual and high performing footwear and apparel.
Deckers Outdoor Corp has a ROE ratio of 27.99%, which is thrashing the industry median of 0.75%.
The share price has increased by 66.62% over the past year up to $269.19 at close on Wednesday for a market capitalization of $7.56 billion and a 52-week range of $78.70 to $285.28.
The stock has a price-book ratio of 6.08 (versus the industry median of 0.97) and a price-earnings ratio of 24.45 (versus the industry median of 18.11).
GuruFocus has assigned a score of 8 out of 10 for both the company's financial strength rating and its profitability rating.
On Wall Street, the stock has a buy recommendation rating with an average target price of $304 per share.
Chemed Corp 
The third stock to have a look at is Chemed Corp (NYSE:CHE), a Cincinnati, Ohio-based company that provides hospice and palliative care services to U.S. patients through a network of physicians and other professionals.
Chemed Corp has a ROE ratio of 37.32%, while the industry has a median of 3.85%.
The share price has risen by 10% over the past year up to $476.69 at close on Wednesday for a market capitalization of $7.6 billion and a 52-week range of $330.01 to $528.29.
The price-book ratio is 9.49 (versus the industry median of 2.9) and the price-earnings ratio is 28.91 (versus the industry median of 29.78).
GuruFocus has assigned a score of 8 out of 10 to the financial strength rating and a score of 9 out of 10 to the profitability rating of the company.
On Wall Street, the stock has a buy recommendation rating with an average target price of $565 per share.