#!/bin/python
import math
import datetime
import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
import traceback
import pdb
import json
import time
from datetime import date
import yfinance as yf
import numpy as np
import pandas as pd
import talib
from pandas_datareader import data as pdr

class volftagmath():
    def __init__(self, ticker, data = None, past_x_samples = 10, price_threshold = 10):
        self.ticker = ticker
        self.data = data
        self.prev_len = 0
        self.avg_price = []
        self.neg_sample_history = []
        self.pos_sample_history = []
        self.int_to_str = ["Z", "T", "M", "K", ""]
        self.cont_zeros_list = []
        self.past_x_samples = past_x_samples
        self.price_thres = price_threshold
        self.num_vol_incr = 0
        self.num_vol_decr = 0
        self.vol_incr_history = []
        self.vol_decr_history = []

    def __del__(self):
        del self.data

    def block_stdout(self):
        f = open(os.devnull, 'w')
        self.out = sys.stdout
        sys.stdout = f

    def unblock_stdout(self):
        f = sys.stdout
        if not hasattr(self, 'out'):
            return
        sys.stdout = self.out
        f.close()
        del self.out

    '''
        price_type : 0= Average Price: (Hight + low )/2
        prince_type : 1 = Open Price
        price_type : 2 = Close Price
        price_type : 3 = High Price
        price_type : 4 = Low Price
    '''
    def get_stock_info(self, start_d = "", end_d = "", auto_adjust = False, interval_a = None, price_type = 0, ignore_candle = True):
        yf.pdr_override()
        self.block_stdout()
        if self.prev_len != 0:
            self.prev_len = len(self.data)

        try:
            if interval_a:
                data = pdr.get_data_yahoo(self.ticker, start = start_d, end = end_d, Threads= False, auto_adjust = auto_adjust, interval = interval_a)
            else:
                data = pdr.get_data_yahoo(self.ticker, start = start_d, end = end_d, auto_adjust = auto_adjust, Threads= False)
        except Exception as inst:
            self.unblock_stdout()
            print(type(inst))
            print(inst.args)
            print(inst)
            return False

        if self.prev_len != 0:
            self.data = self.data.append(data)
        else:
            self.data = data

        for i in range(self.prev_len, len(self.data)):
            if price_type == 0:
                self.avg_price.append((self.data.High[i] + self.data.Low[i])/2)
            elif price_type == 1:
                self.avg_price.append(self.data.Open[i])
            elif price_type == 2:
                self.avg_price.append(self.data.Close[i])
            elif price_type == 3:
                self.avg_price.append(self.data.High[i])
            elif price_type == 4:
                self.avg_price.append(self.data.Low[i])
            else:
                self.avg_price.append(0)
        self.unblock_stdout()
        if len(self.data.Open) == 0:
            return False
        self.prev_len = len(self.data)
        if ignore_candle == False:
            self.candle_names = talib.get_function_groups()['Pattern Recognition']
            for candle in self.candle_names:
                self.data[candle] = getattr(talib, candle)(self.data.Open, self.data.High, self.data.Low, self.data.Close)
        return True

    def get_int_to_str_mill(self, number):
        count = 0
        div = 1000000000000
        if number == 0:
            return "0"
        while number:
            num_tmp = int(number/div)
            if num_tmp == 0:
                count += 1
                div = div/1000
            else:
                return "%d %s"%(num_tmp, self.int_to_str[count])

    def get_average_volume(self, start, end, consider_zeros = False, ignore_above = 500000000):
        average = 0
        count = 0
        for i in range(start, end):
            if self.data.Volume[i] > ignore_above:
                continue
            if consider_zeros == True:
                average = average + self.data.Volume[i]
                count = count + 1
            else:
                if self.data.Volume[i] != 0:
                    average = average + self.data.Volume[i]
                    count = count + 1
        if count == 0:
            return 0
        return average/count
            
    #Get the number of 0 volumes and the index till the value is 0
    def get_zero_price_index_value(self, index):
        zero_volumes = 0
        i = index
        for i in range(index, len(self.data) - 1):
            if self.data.Volume[i] != 0:
                return [i, zero_volumes]
            zero_volumes = zero_volumes + 1
        return [i, zero_volumes]

    def get_nonzero_volume_more_than_avg(self, index, threshold, avg, max_zeros):
        above_avg_volumes = 0
        start_above_avg = 0
        i = index
        tmp_thr = 0
        num_zeros = 0
        if self.data.Volume[i] < avg:
            return None

        for i in range(index, len(self.data) - 1):
            if self.data.Volume[i] == 0:
                if num_zeros > max_zeros:
                    break
                else:
                    num_zeros += 1
            if self.data.Volume[i] < avg:
                tmp_thr += 1
                if tmp_thr > threshold:
                    break
            else:
                if (i - index) <= threshold:
                    start_above_avg += 1
                above_avg_volumes += 1
                tmp_thr = 0
                num_zeros = 0
        if i <= index + threshold:
            return None
        return [i, above_avg_volumes, start_above_avg]

    def get_previous_sample_set_volume(self, start, consider_zeros = False):
        if len(self.cont_zeros_list) == 0:
            return 0
        end_prev = self.cont_zeros_list[-1][1]
        return self.get_average_volume(end_prev, start, consider_zeros)

    #Get the average volume of past num samples
    def get_avg_volume_of_last_n_samples(self, index, num):
        if index < num:
            from_index = 0
        else:
            from_index = index - num
        avg_vol = 0
        for i in range(from_index, index):
            avg_vol = avg_vol + self.data.Volume[i]

        if from_index == index:
            return avg_vol
        else:
            return (avg_vol/(index - from_index))

    #This Function does multiple THings:
    #1. This will how many 0 volumes does the dataset take in order for the price to start reducing
    def analyze_zero_merics(self, min_zero_samples, threshold = 2):
        if len(self.data) == 0:
            return None
        self.avg_volume = self.get_average_volume(0, len(self.data))
        num_z_decs = 0
        num_z_incr = 0
        num_neg_samples = 0
        num_positive_samples = 0
        i = 0
        #Get After how many 0 volume samples the price reduces.
        while i < len(self.data):
            if self.data.Volume[i] == 0:
                ans = self.get_zero_price_index_value(index = i)
                if ans[1] < min_zero_samples:
                    if i == ans[0]:
                        i = ans[0] + 1
                    else:
                        i = ans[0] 
                    continue
                if self.avg_price[ans[0]] >= self.avg_price[i] and (self.avg_price[ans[0]] - self.avg_price[i]) > self.price_thres:
                    num_neg_samples = num_neg_samples + 1
                    num_z_incr += ans[1]
                    self.neg_sample_history.append( ["%s to %s"%(str(self.data.Open._index[i]), str(self.data.Open._index[ans[0]])), "%d -> %d"%(self.avg_price[i], self.avg_price[ans[0]]), self.avg_price[ans[0]] -  self.avg_price[i] , ans[1], self.get_int_to_str_mill(self.get_previous_sample_set_volume(i, True)), self.get_previous_sample_set_volume(i, True), self.get_int_to_str_mill(self.get_avg_volume_of_last_n_samples(i, self.past_x_samples))] )
                elif self.avg_price[ans[0]] < self.avg_price[i] and (self.avg_price[i] - self.avg_price[ans[0]]) > self.price_thres:
                    num_positive_samples = num_positive_samples + 1
                    num_z_decs = (ans[0] - i) + num_z_decs
                    self.pos_sample_history.append( ["%s to %s"%(str(self.data.Open._index[i]), str(self.data.Open._index[ans[0]])), "%d -> %d"%(self.avg_price[i], self.avg_price[ans[0]]), self.avg_price[i] -  self.avg_price[ans[0]] , ans[1], self.get_int_to_str_mill(self.get_previous_sample_set_volume(i, True)), self.get_previous_sample_set_volume(i, True), self.get_int_to_str_mill(self.get_avg_volume_of_last_n_samples(i , self.past_x_samples))] )
                self.cont_zeros_list.append([i, ans[0]])
                
                i = ans[0]
                continue
            i += 1
        self.num_z_incr = num_z_incr
        self.num_z_decs = num_z_decs
        self.num_neg_samples = num_neg_samples
        self.num_positive_samples = num_positive_samples
        self.neg_sample_history.sort(key = lambda x:x[2])
        self.pos_sample_history.sort(key = lambda x:x[2])


    #Check if volume greater than avg
    def find_volume_greater(start_i, end_i,  curr_vol):
        avg = self.get_average_volume(start_i, end_i)
        if curr_vol > avg:
            return True
        return False

    #Check if the price after threshold samples has increased/decreased/invalid.
    def check_sample_trend(self, index, threshold, size):
        if size < threshold or (index + threshold) >= len(self.data):
            return "N/A"
        if self.avg_price[index] < self.avg_price[index + threshold]:
            return "Increasing"
        elif self.avg_price[index] - self.avg_price[index + threshold] < 1:
            return "Almost Same"
        else:
            return "Decreasing"

    def get_trend_of_list(self, from_i, to_i):
        if to_i - from_i <= 1:
            return "0"

        df = pd.DataFrame({'x_axis': list(range(0, to_i - from_i+ 1)), 'y_axis': self.avg_price[from_i:to_i+1]})
        data = df['y_axis']
        coefficients, residuals, _, _, _ = np.polyfit(range(0, to_i - from_i + 1), self.avg_price[from_i:to_i + 1], 1, full = True)
        
        return str(coefficients[0])
        

    #Find the instances when the volume hike has caused an increase in the prices
    def check_if_volume_constant(self, threshold, moving_samples = 1440):
        if len(self.data) == 0:
            return None
        i = moving_samples
        
        while i < len(self.data):
            avg = self.get_average_volume(i - moving_samples, i, consider_zeros = False)/4
            if self.data.Volume[i] > avg:
                ans = self.get_nonzero_volume_more_than_avg(i,  threshold, avg, threshold/2)
                if ans == None:
                    i += 1
                    continue
                #Populate the indices
                check_from_i = i + threshold
                check_to_i = ans[0]
                check_from = self.avg_price[check_from_i]
                check_to = self.avg_price[check_to_i]

                if check_from < check_to and (check_to - check_from) > self.price_thres:
                    self.num_vol_incr += 1
                    self.vol_incr_history.append([str(self.data.Open._index[i]), i, ans[0], "%s to %s"%(str(self.data.Open._index[i]), str(self.data.Open._index[check_to_i])), "%d -> %d"%(check_from, check_to), check_to -  check_from, ans[1], check_to_i - i, ans[2], self.get_int_to_str_mill(avg), self.check_sample_trend(i, threshold, check_to_i - check_from_i), self.get_trend_of_list(check_from_i - 20, check_from_i)])
                elif check_from > check_to and (check_from - check_to) > self.price_thres:
                    self.num_vol_decr += 1
                    self.vol_decr_history.append([str(self.data.Open._index[i]),i, ans[0], "%s to %s"%(str(self.data.Open._index[i]), str(self.data.Open._index[check_to_i])), "%d -> %d"%(check_from, check_to), check_from -  check_to, ans[1], check_to_i - i, ans[2],  self.get_int_to_str_mill(avg), self.check_sample_trend(i, threshold, check_to_i - check_from_i), self.get_trend_of_list(check_from_i - 20, check_from_i)])
                i = ans[0]
                continue
            i += 1
        

    
