#!/bin/python3
import threading
from multiprocessing import Process, Queue
import math
import datetime
import os, sys
from get_all_tickers import get_tickers as gt
import traceback
import pdb
import json
import time
import glob
import pickle
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
sys.path.append(os.environ["POLYGON_LIB_DIR"])
import polylibs
from stockbaba_lib import volftaglib
from stockbaba_lib import volftag_math
from stockbaba_lib import sam_talib
from termcolor import colored as cl
import matplotlib.pyplot as plt
from stockbaba_lib import sendmail
import yfinance as yf
from pandas_datareader import data as pdr
from stockbaba_lib import dataframfile

plt.rcParams['figure.figsize'] = (20, 10)
plt.style.use('fivethirtyeight')

input_args = {}

#In the Future, the plan is to Run this Script every 30 mins. So, this class will help Achieve that.
class timed_sleep:
    def __init__(self, sleep_time = 60, wakeup_time = "30m"):
        self.sleep_time = sleep_time
        self.wakeup_time = wakeup_time
        self.timed_list = None

    def populate_times(self):
        if self.timed_list != None:
            del self.timed_list

        if self.wakeup_time == "15m":
            self.timed_list = ["09:15:00", "09:30:00", "09:45:00", "10:00:00", "10:15:00", "10:30:00", "10:45:00", "11:00:00", "11:15:00", "11:30:00", "11:45:00", "12:00:00", "12:15:00", "12:30:00", "12:45:00", "13:00:00", "13:15:00", "13:30:00", "13:45:00", "14:00:00", "14:15:00", "14:30:00", "14:45:00", "15:00:00", "15:15:00"]
        elif self.wakeup_time == "30m":
            self.timed_list = ["09:30:00", "10:00:00", "10:30:00", "11:00:00", "11:30:00", "12:00:00", "12:30:00", "13:00:00", "13:30:00", "14:00:00", "14:30:00", "15:00:00"]
        elif self.wakeup_time == "1h":
            self.timed_list = ["10:00:00", "11:00:00", "12:00:00", "13:00:00", "14:00:00", "15:00:00"]

    def sleep_till_time(self, next_wake_time, next_day = False):
        if next_day == True:
            tom = datetime.datetime.now() + datetime.timedelta(days=1)
            tom_date = tom.strftime("%y-%m-%d")
            while True:
                now = datetime.datetime.now()
                curr_date = now.strftime("%y-%m-%d")
                if curr_date < tom_date:
                    time.sleep(self.sleep_time)
                else:
                    break

        while True:
            now = datetime.datetime.now()
            current_time = now.strftime("%H:%M:%S")
            if current_time > next_wake_time:
                print("Woke up at %s"%current_time)
                return True
            time.sleep(self.sleep_time)

    def sleep_till_next(self):
        if self.timed_list == None or len(self.timed_list) == 0:
            self.populate_times()
            now = datetime.datetime.now()
            current_time = now.strftime("%H:%M:%S")
            if current_time > "15:30:00":
                self.sleep_till_time("09:15:00", next_day = True)
            elif current_time < "09:15:00":
                self.sleep_till_time("09:15:00")

        now = datetime.datetime.now()
        current_time = now.strftime("%H:%M:%S")
        next_wake_time =  self.timed_list[0]
        while current_time >  next_wake_time:
            if len(self.timed_list) == 0:
                print("All today's samples are over. Come tomorrow")
                return False
            next_wake_time = self.timed_list.pop(0)
        self.sleep_till_time(next_wake_time)


#This was an intermediate API I used to analyze the Data. No more used.
def analyze_ta_data(ta_data):
    macd = ta_data.macd
    data = ta_data.volftag.data
    bullstics = ta_data.bullstics
    print("======================= %s ==================="%ta_data.volftag.ticker)
    profit = 0
    loss = 0
    for i in range(len(bullstics.up_crossings)):
        from_index = bullstics.up_crossings[i][0]
        to_index = bullstics.up_crossings[i][1]
        print("From %s to %s, Price Increase %f , Price Decrease %f "%(str(data.Open._index[from_index + 1]), str(data.Open._index[to_index + 1]), bullstics.up_cross_max_min[i][1], bullstics.up_cross_max_min[i][0]))
        profit += bullstics.up_cross_max_min[i][1]
        loss += bullstics.up_cross_max_min[i][0]
    print("Number of negative short impulses %d"%bullstics.neg_samples)
    print("Current Price: %f"%ta_data.volftag.data.Close[-1])
    print("Analysis: Total Profit = %f, Total Loss = %f"%(bullstics.total_profit, bullstics.total_loss))
    print("Net Profit = %f"%(bullstics.total_profit - bullstics.total_loss))
    print("Average Percentage Profit: %f"%bullstics.avg_profit_perc)
    print("Average Percentage Loss: %f"%bullstics.avg_loss_perc)
    print("Average Percentage of times the threshold was crossed :%d"%bullstics.avg_num_profit_cross_threshold)
    

#Read the Readme.md file for more details on the architecture.
#This is the API that will Fork Processes one by one to save Memory. If you have ~50-60GB RAM then please make it concurrent.
def do_analysis():
    global input_args
    start_index = 0
    batch_size = 500
    end_tmp = batch_size
    fork = False

    #Setup input Arguements
    f = open ('input.json', "r")
    input_args = json.loads(f.read())
    f.close()

    #Setup Email System
    mail_info = sendmail.gmail(sender_mail = input_args['email_info']['sender'], passwd_file = input_args['email_info']['passwd_file'])
    for recvr in input_args['email_info']["receiver"]:
        mail_info.add_receiver(recvr)
    add_clear_mail_info(clear = True)

    #Get the History Data 
    history_len = len(glob.glob("stock_data/stock_history_*"))
    if history_len == 0:
        print("The history is not taken. Please create history")
        sys.exit(0)

    #Do the forking and Enjoy
    for i in range(0, history_len):
        filename = "stock_history_%d_%d"%(start_index, end_tmp)
        list_file = "stock_list_%d_%d"%(start_index, end_tmp)
        ta_data_file = "ta_data_%d_%d"%(start_index, end_tmp)
        print("Processing the info and saving info to file : %s"%filename)

        start_index += batch_size
        end_tmp += batch_size
        if fork == True:
            p = Process(target=do_analysis_api, args=(input_args['file_save']['start-date'], input_args['file_save']['end-date'], input_args['file_save_daily']['start-date'], input_args['file_save_daily']['end-date'], filename, list_file, fork, ta_data_file, "./stock_data",))
            p.start()
            p.join()
        else:
            do_analysis_api(input_args['file_save']['start-date'], input_args['file_save']['end-date'], input_args['file_save_daily']['start-date'], input_args['file_save_daily']['end-date'], filename, list_file, fork, ta_data_file, data_dir = "./stock_data")
    send_mail_to_all(mail_info)

#This will get the Stored object that was used for techical Analysis in Earlier iterations.
#If it was not stored ,then it will create it.
def get_technical_analysis_object(start, end, d_start, d_end, stock_file, list_file, ta_list_file, time_gap, time_unit, data_dir):
    # If the ta_analysis object already exists then just send it.
    ta_f_name = data_dir + "/" + ta_list_file
    if os.path.isfile(ta_f_name) == True:
        filehandler = open(ta_f_name, 'rb')
        tech_analysis = pickle.load(filehandler)
        filehandler.close()
        return tech_analysis

    #Calculate Everything
    l_file = data_dir + "/" + list_file
    if os.path.isfile(l_file) == False:
        print("The list file does not exist")
        sys.exit(1)
    filehandler = open(l_file, 'rb')
    stocklist = []
    stocklist = pickle.load(filehandler)

    tech_analysis = sam_talib.samtalib()
    data_stored = dataframfile.data_file(stock_list = stocklist, start = start, end = end, d_start = d_start, d_end = d_end, time_gap = time_gap, time_unit = time_unit, threads=True, data_dir = data_dir, data_file = stock_file, list_file = list_file)
    pandas_list = data_stored.p_list
    daily_list = data_stored.d_list
    del stocklist
    stocklist = data_stored.final_list

    print("Total Number of stocks: %d"%(len(stocklist)))
    for ele in stocklist:
        #print("Analyzing the stock: %s"%ele)
        #sys.stdout.flush()
        if pandas_list[ele] is None or 'Open' not in pandas_list[ele]:
            continue
        if daily_list[ele] is None:
            tech_analysis.add_ticker_info(ele, pandas_list[ele]['Open'], pandas_list[ele]['High'], pandas_list[ele]['Low'], pandas_list[ele]['Close'], pandas_list[ele]['Volume'], None, None, None, None)
        else:
            tech_analysis.add_ticker_info(ele, pandas_list[ele]['Open'], pandas_list[ele]['High'], pandas_list[ele]['Low'], pandas_list[ele]['Close'], pandas_list[ele]['Volume'], daily_list[ele]['Open'], daily_list[ele]['High'], daily_list[ele]['Low'], daily_list[ele]['Close'])
        del  pandas_list[ele]['Open'], pandas_list[ele]['High'], pandas_list[ele]['Low'], pandas_list[ele]['Close'], pandas_list[ele]['Volume']
        tech_analysis.fill_historic_data(tech_analysis.tickers[ele])

    del data_stored
    del pandas_list

    #Save the object
    ta_f_name = data_dir + "/" + ta_list_file
    filehandler = open(ta_f_name, 'wb')
    pickle.dump(tech_analysis, filehandler)

    # This is the Return Value
    return tech_analysis

def poly_download_all_data(ticker_list, start, end, time_gap, time_unit):
    poly = polylibs.polygon_lib(apikey = os.environ["POLYGON_IO_API_KEY"], debug = False)
    pandas_list = poly.fetch_all_info(ticker_list, start = start, end = end, time_gap = time_gap, time_unit = time_unit)
    return pandas_list

def do_analysis_api(start, end, d_start, d_end, stock_file, list_file, fork, ta_list_file, data_dir):
    global input_args
    time_gap = input_args['time_gap']
    time_unit = input_args['time_unit']

    tech_analysis = get_technical_analysis_object(start, end, d_start, d_end, stock_file, list_file, ta_list_file, time_gap, time_unit, data_dir)
    ta_list = tech_analysis.list
    #halt_infra = timed_sleep()
    send_str = ""
    #sys.stdout.flush()
    #halt_infra.sleep_till_next()
    #time.sleep(300)
    #yf.pdr_override()

    if send_str != "":
        del send_str
        send_str = ""

    pandas_list = poly_download_all_data(ticker_list = ta_list, start = input_args['carjack']['start-date'], end = input_args['carjack']['end-date'], time_gap = input_args['time_gap'], time_unit = input_args['time_unit'])
    daily_list = poly_download_all_data(ticker_list = ta_list, start = input_args['carjack']['day-start'], end = input_args['carjack']['day-end'], time_gap = "1", time_unit = "day")
    for ticker in ta_list:
        ele = tech_analysis.tickers[ticker]
        if pandas_list[ticker] is None or 'Open' not in pandas_list[ticker]:
            continue
        if daily_list[ticker] is None:
            tech_analysis.add_ticker_info(ticker, pandas_list[ticker]['Open'], pandas_list[ticker]['High'], pandas_list[ticker]['Low'], pandas_list[ticker]['Close'], pandas_list[ticker]['Volume'], None, None, None, None)
        else:
            tech_analysis.add_ticker_info(ticker, pandas_list[ticker]['Open'], pandas_list[ticker]['High'], pandas_list[ticker]['Low'], pandas_list[ticker]['Close'], pandas_list[ticker]['Volume'], daily_list[ticker]['Open'], daily_list[ticker]['High'], daily_list[ticker]['Low'], daily_list[ticker]['Close'])
        tech_analysis.fill_historic_data(ele)
        tech_analysis.string_add_stock_info(ele)
        send_str = send_str + ele.mail_str
    if send_str != "":
        add_clear_mail_info(send_str = send_str)
    else:
        print("The Cycle was run again and there was nothing found to send via mail")

    ta_f_name = data_dir + "/" + ta_list_file
    filehandler = open(ta_f_name, 'wb')
    pickle.dump(tech_analysis, filehandler)
    
    if fork == True:
        sys.exit(0)
    else:
        return
    #halt_infra.sleep_till_next()

def add_clear_mail_info(filename = "./send_mail_info", send_str = "", clear = False):
    if clear == True:
        if os.path.isfile(filename) == True:
            os.remove(filename)
        return
    file_object = open(filename, 'a')
    file_object.write(send_str)
    file_object.close()

def send_mail_to_all(mail_info, filename = "./send_mail_info"):
    if os.path.isfile(filename) == False:
        return

    file_object = open(filename, 'r')
    string_mail = file_object.read()
    file_object.close()
    if len(string_mail) == 0:
        return

    mail_info.send_mail(string_mail, "Project Carjack ")
    

def add_cgroup(memlimit = 5 * 1024 * 1024 * 1024):
    if os.path.isdir('/sys/fs/cgroup/memory/5gb_limit/') == False:
        os.mkdir('/sys/fs/cgroup/memory/5gb_limit/')
    f = open("/sys/fs/cgroup/memory/5gb_limit/memory.limit_in_bytes", "a")
    f.write(str(memlimit))
    f.close()
    f = open("/sys/fs/cgroup/memory/5gb_limit/cgroup.procs", "a")
    f.write(str(os.getpid()))
    f.close()

if __name__ == '__main__':
    add_cgroup(memlimit = 5 * 1024 * 1024 * 1024)
    do_analysis()

