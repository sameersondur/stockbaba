#!/bin/python3
import threading
from multiprocessing import Process, Queue
import math
import datetime
import os, sys
import traceback
import pdb
import json
import time
import glob
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
from stockbaba_lib import dataframfile
sys.path.append(os.environ["POLYGON_LIB_DIR"])
import polylibs

def test_files():
    poly = polylibs.polygon_lib(apikey = os.environ["POLYGON_IO_API_KEY"])
    stocklist = poly.get_all_tickers()
    del poly
    stocklist = [i.strip(' ') for i in stocklist]
    stocklist = [i.replace('^', '-P') for i in stocklist]
    stocklist = [i.replace('/', '-') for i in stocklist]
    #Setup input Arguements
    f = open ('input.json', "r")
    input_args = json.loads(f.read())
    f.close()

    start_index = 0
    end_index = len(stocklist) - 1
    batch_size = 500
    end_tmp = batch_size

    if len(stocklist) <= batch_size:
        loop_len = 1
        end_tmp = len(stocklist)
    else:
        loop_len = int(end_index/batch_size) + 1

    for i in range(0, loop_len):
        if (start_index > end_index):
            break

        filename = "stock_history_%d_%d"%(start_index, end_tmp)
        list_file = "stock_list_%d_%d"%(start_index, end_tmp)
        if end_tmp > end_index + 1:
            end_tmp = end_index
        print("Processing the info and saving info to file : %s"%filename)
        data = dataframfile.data_file(stock_list = stocklist[start_index:end_tmp], start = input_args['file_save']['start-date'], end = input_args['file_save']['end-date'], d_start = input_args['file_save_daily']['start-date'], d_end = input_args['file_save_daily']['end-date'], time_gap = input_args['time_gap'], time_unit = input_args['time_unit'], threads=False, data_dir = input_args['data_dir'], data_file = filename, list_file = list_file, min_price = input_args['min_price'])
        del data
        start_index += batch_size
        end_tmp += batch_size

if __name__ == '__main__':
    test_files()
