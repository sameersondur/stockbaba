#!/bin/python3
import threading
from multiprocessing import Process, Queue
import math
import datetime
import os, sys
from get_all_tickers import get_tickers as gt
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
from stockbaba_lib import get_stock_info_lib
from stockbaba_lib import stock_news
import traceback
import pdb
import json
import time
import glob
import csv
import shutil
from stockbaba_lib import sentilib
#from pdb_clone import pdbhandler

#GLoabal Variables:
input_args = None
total_num_stocks = 0
workerlist = []
sentilib_t = None

class worker_forks:
    def __init__(self, ticker_list, name, from_index, to_index, arguements):
        self.name = name
        self.from_ind = int(from_index)
        self.to_ind = int(to_index)
        self.input_args = arguements
        self.ticker_list = ticker_list
        self.out_filename = "sentiquity_output_%s"%(self.name)
        self.json_file_name = "sentiquity_json_%s"%(self.name)
        self.json_data = {}
        self.json_data['sentiquity'] = []
        self.json_data['total_sentiquity'] = []
        #The sql server Info of the database
        self.sentilib = sentilib.data_lib(  host = input_args['mysql_info']['host'], sql_user = input_args['mysql_info']['user'], sql_passwd = input_args['mysql_info']['password'], 
                                            my_email = input_args['gmail_info']['myemail'], passwd_file= input_args['gmail_info']['password_file'], 
                                            gsheet_scope = input_args['gsheet_info']['scope'], gsheet_id = input_args['gsheet_info']['sheet_id'], range_capture = 'Form Responses 1!A:Z')


    #yfinance APIs are not Thread safe, they throw all kinds of exceptions when in multithreaded environment.
    #Will be using multiple forks to isolate the yfinace calls and avoid contention.
    def worker(self):
        #sys.stdout = open(self.out_filename, 'w')
        print('Parent process:', os.getppid())
        print('Process id:', os.getpid())
        #ForkedPdb().set_trace()
        for i in range(self.from_ind, self.to_ind):
            print("Downloading info of %s Thread name %s"%(self.ticker_list[i], self.name))
            sys.stdout.flush()
            try:
                stock = stock_news.stock_news(ticker = self.ticker_list[i], max_links = 5, index = i)
            except:
                print("Exception raised for ticker %s"% self.ticker_list[i])
                continue
            if stock == None:
                print("Could not find stock %s"%self.ticker_list[i])
                continue
            stock.print_info()
            stock.print_json()
            
        sys.stdout.flush()
        

    #Prepare for Fork Arguements
    def worker_create(self):
        self.fork = Process(target=self.worker, args=())
        if self.fork == None:
            return False
        return True


def open_input_args():
    global input_args
    with open('./input.json') as input_file:
        input_args = json.load(input_file)

def clear_output():
    if not os.path.exists('output'):
        os.makedirs('output')
    else:
        shutil.rmtree('output')
        os.makedirs('output')

def update_the_sheets_and_customer_table(input_args):
    global sentilib_t
    sentilib_t = sentilib.data_lib(  host = input_args['mysql_info']['host'], sql_user = input_args['mysql_info']['user'], sql_passwd = input_args['mysql_info']['password'],
                                my_email = input_args['gmail_info']['myemail'], passwd_file= input_args['gmail_info']['password_file'],
                                gsheet_scope = input_args['gsheet_info']['scope'], gsheet_id = input_args['gsheet_info']['sheet_id'], 
                                range_capture = 'Form Responses 1!A:Z')
    sentilib_t.check_new_entry_in_sheets()

def get_sentiquity():
    disable_fork = True 
    global workerlist
    global input_args
    global sentilib_t
    #list_of_tickers = gt.get_tickers()
    #These are the stocks that are good, that I know of and can be used for validatiosn.
    start_index = 0
    update_the_sheets_and_customer_table(input_args)
    list_of_tickers = sentilib_t.get_the_final_list_of_tickers()
    total_num_stocks = len(list_of_tickers)
    del sentilib_t

    max_tickers_per_worker = 500
    for i in range(total_num_stocks//max_tickers_per_worker + 1):
        start_index = i*max_tickers_per_worker
        if total_num_stocks < (i+1)*max_tickers_per_worker:
            end_index = total_num_stocks
        else:
            end_index = (i+1)*max_tickers_per_worker

        #Making Worker Variables
        worker_name = "Hunter-%d"%(i)
        worker_c = worker_forks(name = worker_name, ticker_list = list_of_tickers, from_index = start_index, to_index = end_index, arguements = input_args)
        if disable_fork == False:
            if worker_c.worker_create() == False:
                print("%s could not be created. Index %d to %d cannot be preoceesed."%(worker_name, start_index, end_index))
                del worker_c
                continue

        workerlist.append(worker_c)
        if end_index == total_num_stocks:
            break
    
    if disable_fork == True:
        workerlist[i].worker()
    else:
        for i in range(len(workerlist)):
            workerlist[i].fork.start()
        for i in range(len(workerlist)):
            workerlist[i].fork.join()

if __name__ == '__main__':
    #pdbhandler.register()
    open_input_args()
    clear_output()
    get_sentiquity()
