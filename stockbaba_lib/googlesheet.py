from __future__ import print_function
import pickle
import os.path
from googleapiclient import discovery
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from pprint import pprint

class gsheets():
    def __init__(self, scope, spreadsheet_id, range_capture, cred_file = '/tmp/credentials.json_', token_file = '/tmp/token.pickle_'):
        self.init_done = False
        self.scope = scope
        self.spreadsheet_id = spreadsheet_id
        self.cred_file = cred_file+spreadsheet_id
        self.token_file = token_file+spreadsheet_id
        self.range = range_capture
        self.creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if not os.path.exists(self.cred_file):
            print("The credential File %s does not exists."%self.cred_file)
            return
        if os.path.exists(self.token_file):
            with open(self.token_file, 'rb') as token:
                self.creds = pickle.load(token)
        if not self.creds or not self.creds.valid:
            if self.creds and self.creds.expired and self.creds.refresh_token:
                print(self.creds.expired)
                print(self.creds.refresh_token)
                #os.remove(self.token_file)
                self.creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    self.cred_file, self.scope)
                self.creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open(self.token_file, 'wb') as token:
                pickle.dump(self.creds, token)
        self.service = build('sheets', 'v4', credentials=self.creds)
        self.spreadsheets = self.service.spreadsheets()
        self.init_done = True

    def get_all_rows(self):
        result = self.spreadsheets.values().get(spreadsheetId=self.spreadsheet_id, range = self.range).execute()
        values = result.get('values', [])
        return values

    def clear_entries(self, row_num, col_start, col_end, sheet_name):
        buff = "%s!%s%d:%s%d"%(sheet_name, col_start, row_num, col_end, row_num)
        self.spreadsheets.values().clear(spreadsheetId = self.spreadsheet_id,
                                range= buff).execute()
