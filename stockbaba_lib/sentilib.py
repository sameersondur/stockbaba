#!/bin/python
import math
import datetime
import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
from stockbaba_lib import get_stock_info_lib
from stockbaba_lib import stock_news
import traceback
import pdb
import json
import time
from stockbaba_lib import database
from stockbaba_lib import sendmail
from stockbaba_lib import googlesheet
import ast
from heapq import merge
from datetime import date

customer_table = """ customers (
    email varchar(255) NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255) NOT NULL,
    tickers JSON,
    timeout_date DATETIME,
    PRIMARY KEY (email)
);
"""
customer_tag = "customers (email, LastName, FirstName, tickers, timeout_date) VALUES(%s, %s, %s, %s, %s)"

ticker_table = """ tickers (
    ticker varchar(255) NOT NULL,
    company_name varchar(1024),
    sector varchar(1024),
    num_dividends int,
    num_positive_splits int,
    finviz_url varchar(1024),
    num_articles int,
    from_article_date varchar(1024),
    to_article_date varchar(1024),
    count int,
    articles_info json,
    PRIMARY KEY (ticker)
);
"""
ticker_table_tag = "tickers (ticker, company_name, sector, num_dividends, num_positive_splits, finviz_url, num_articles, from_article_date, to_article_date, count, articles_info) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"


class data_lib():
    def __init__(self, host, sql_user, sql_passwd, my_email, passwd_file, gsheet_scope, gsheet_id, range_capture):
        self.database = database.database(host = host, user = sql_user, password = sql_passwd)
        self.database.create(d_name = "sentiquity")
        self.database.enter_database_context(dbase_name = "sentiquity")
        self.database.create_table(table_string = customer_table)
        self.database.create_table(table_string = ticker_table)
        self.sendmail = sendmail.gmail(sender_mail = my_email, passwd_file = passwd_file)
        self.mail_content = sendmail.mail_content()
        self.sheets = googlesheet.gsheets(scope = [gsheet_scope], spreadsheet_id = gsheet_id,
                                            range_capture = range_capture)

    def __del__(self):
        del self.database
        del self.sendmail
        del self.mail_content 
        del self.sheets

    def get_listandtime(self, p_key_present, email_id):
        if p_key_present == False:
            message = self.mail_content.not_subscribed()
            self.sendmail.send_single_mail(receiver = email_id, message = message)
        else:
            tickers_str = self.database.get_table_entry(table = "customers", key = "email", value = "\"%s\""%email_id)[0][3]
            tickers = json.loads(tickers_str)
            ticker_list = ""
            count = 0
            for ticker in tickers['tickerlist']:
                if len(ticker) > 0:
                    ticker_list = ticker_list + ticker + "\n"
                    count = count + 1
                    
            if count == 0:
                ticker_list = "No Tickers in your list"
            print("Shanta len %d "%len(tickers['tickerlist']))
            print(tickers['tickerlist'])
            message = self.mail_content.ticker_list_request(tickers = ticker_list)
            self.sendmail.send_single_mail(receiver = email_id, message = message)

    def ticker_arr_to_jsonstr(self, values):
        json_base = {
            'tickerlist': []
        }
        for element in values:
            json_base['tickerlist'].append(element)
        return json.dumps(json_base)

    def ticker_jsonstr_to_arr(self, json_str):
        jsonobj = json.loads(json_str)
        ret_array = []

        if isinstance(jsonobj['tickerlist'], str):
            jsonobj['tickerlist'] = json.loads(ast.literal_eval(jsonobj['tickerlist']))

        for element in jsonobj['tickerlist']:
            ret_array.append(element)
        del jsonobj
        return ret_array

    def add_tickers_to_list(self, ticker_list, p_key, p_val):
        entry = self.database.get_table_entry(table = "customers", key= p_key, value = p_val)[0]
        original_list_str = entry[3]
        jsonobj = json.loads(original_list_str)

        if isinstance(jsonobj['tickerlist'], str):
            jsonobj['tickerlist'] = json.loads(jsonobj['tickerlist'].replace("\'", "\""))

        jsonobj['tickerlist'].sort()
        ticker_list.sort()
        orig_length = len(jsonobj['tickerlist'])
        jsonobj['tickerlist'] = list(set(jsonobj['tickerlist'] + ticker_list))

        #This means that all the tickers are already present.
        if len(jsonobj['tickerlist']) == orig_length:
            return
        new_list_str = "JSON_SET(tickers, \"$.tickerlist\", \"" + json.dumps(jsonobj['tickerlist']).replace("\"", "\'") + "\")"
        self.database.update_table_values(table = "customers", keys = ["tickers"], values = [new_list_str], primary_key = p_key, primary_val = p_val)

    def delete_tickers_from_list(self, ticker_list, p_key, p_val):
        entry = self.database.get_table_entry(table = "customers", key= p_key, value = p_val)[0]
        original_list_str = entry[3]
        jsonobj = json.loads(original_list_str)

        if isinstance(jsonobj['tickerlist'], str):
            jsonobj['tickerlist'] = json.loads(jsonobj['tickerlist'].replace("\'", "\""))

        if len(jsonobj['tickerlist']) == 0:
            return

        count = 0
        for ticker in ticker_list:
            for ele in jsonobj['tickerlist']:
                if ele == ticker:
                    jsonobj['tickerlist'].remove(ticker)
                    count = count + 1
        if count == 0:
            return
        new_list_str = "JSON_SET(tickers, \"$.tickerlist\", \"" + json.dumps(jsonobj['tickerlist']).replace("\"", "\'") + "\")"
        self.database.update_table_values(table = "customers", keys = ["tickers"], values = [new_list_str], primary_key = p_key, primary_val = p_val)

    def get_date_time(self, time):
        today = datetime.date.today()
        if len(time) == 0:
            time = "9:00 AM"
        am_pm = time.split()[1]
        time_str = time.split()[0]
        if am_pm == "AM":
            real_time = time_str
        else:
            hr_ampm = time_str.split(':')
            if hr_ampm[0] == "12":
                real_time = time_str
            else:
                hr_ampm = time_str.split(':')
                hr_ampm[0] = str(int(hr_ampm[0]) + 12)
                real_time = ':'.join(hr_ampm)
        return str(today) + " " + real_time

    def carry_out_action(self, sheet_entry):
        entry_timestamp = sheet_entry[0]
        target_date_time = self.get_date_time(time = sheet_entry[1])
        action = sheet_entry[2]
        tickers = sheet_entry[3]
        print(tickers)
        email_id = sheet_entry[4]
        if len(sheet_entry) > 5:
            firstname = sheet_entry[5]
        else:
            firstname = ""
        if len(sheet_entry) > 6:
            lastname = sheet_entry[6]
        else:
            lastname = ""
        #Carry Out the Action as Follows
        primarykey_present = self.database.check_if_val_present(table = "customers", key = "email", value = "\"%s\""%email_id)
        print("primarykey_present %d"%primarykey_present)
        if action == "Unsubscribe":
            if primarykey_present == True:
                self.database.delete_table_rows(table = "customers", primary_key = "email", primary_value = "\"%s\""%email_id)
            message = self.mail_content.unsubscribed_success()
            self.sendmail.send_single_mail(receiver = email_id, message = message)
        elif action == "Get my current list and time details":
            if primarykey_present == True:
                self.get_listandtime(p_key_present = primarykey_present, email_id = email_id)
            else:
                message = self.mail_content.not_subscribed()
                self.sendmail.send_single_mail(receiver = email_id, message = message)
        elif action == "Delete the below tickers from my list":
            if primarykey_present == True:
                self.delete_tickers_from_list(ticker_list = tickers.split(','), p_key ="email", p_val = "\"%s\""%email_id)
            else:
                message = self.mail_content.not_subscribed()
                self.sendmail.send_single_mail(receiver = email_id, message = message)
        elif action == "Add the below tickers to my list":
            if primarykey_present == True:
                self.add_tickers_to_list(ticker_list = tickers.split(','), p_key = "email", p_val = "\"%s\""%email_id)
            else:
                self.add_grow_to_database(sheet_entry = sheet_entry)

    def add_new_customer_entry(self, email_id, lastname, firstname, tickers_str_comma, t_date_time):
        values = []
        values.append(email_id)
        values.append(lastname)
        values.append(firstname)
        values.append(self.ticker_arr_to_jsonstr(values = tickers_str_comma.split(',')))
        values.append(t_date_time)
        self.database.insert_into_table(command_string = customer_tag, val_arr = values, table = "customers", primary_key = "email", primary_val = "\"%s\""%email_id)

    def add_grow_to_database(self, sheet_entry):
        entry_timestamp = sheet_entry[0]
        target_date_time = self.get_date_time(time = sheet_entry[1])
        action = sheet_entry[2]
        tickers = sheet_entry[3]
        email_id = sheet_entry[4]
        firstname = sheet_entry[5]
        lastname = sheet_entry[6]
        self.add_new_customer_entry(email_id = email_id, lastname = lastname, firstname = firstname, tickers_str_comma = tickers, t_date_time = target_date_time)

    def check_new_entry_in_sheets(self):
        values = self.sheets.get_all_rows()
        if len(values) <= 1:
            return
        for i in range(1, len(values)):
            if len(values[i]) == 0:
                continue
            email_id = values[i][4]
            if self.database.check_if_val_present(table = "customers", key = "email", value = "\"%s\""%email_id) == True:
                self.carry_out_action(sheet_entry = values[i])
            else:
                #self.add_grow_to_database(sheet_entry = values[i])
                self.carry_out_action(sheet_entry = values[i])
            self.sheets.clear_entries(row_num = i + 1, col_start = 'A', col_end = 'Z', sheet_name = "Form Responses 1")

    def get_the_final_list_of_tickers(self, from_table = "customers", ticker_index = 3, to_table = "tickers"):
        self.database.enter_database_context("sentiquity")
        table_elements = self.database.get_all_elements_in_table("customers")
        tickerlist = []
        for ele in table_elements:
            jsonobj = json.loads(ele[3])
            if isinstance(jsonobj['tickerlist'], str):
                jsonobj['tickerlist'] = json.loads(jsonobj['tickerlist'].replace("\'", "\""))
            for ticker in jsonobj['tickerlist']:
                tickerlist.append(ticker.lstrip())
    
        tickerlist.sort()
        tickerlist = list(set(tickerlist))
        print("The list of stocks that we are going to look into are:")
        print(tickerlist)
        for ele in tickerlist:
            today = date.today()
            yesterday = today - datetime.timedelta(days=1)
            stock_info = get_stock_info_lib.stock_basic_analysis(stock_ticker = ele, start_d = "2021-09-02", end_d = "2021-09-03", index = 0)
            if stock_info == None:
                continue
            today = date.today()
            
            values = [stock_info.ticker, stock_info.company_name, stock_info.sector_name, 0, 0, "https://finviz.com/quote.ashx?t=%s"%ele, 0, "", "",  0, "{\"info\" : \"\"}"]
            self.database.insert_into_table(command_string = ticker_table_tag, val_arr = values, table = "tickers", primary_key = "ticker", primary_val = "\"%s\""%ele)
            del stock_info
        return tickerlist            

