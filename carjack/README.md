# What do I want to Achieve?
There are 2 parameters in the technical Analysis that are leading parameters that signify that the price is going to increase or decrease. They are as follows: \
MACD(12, 26 9) and RSI \
The goal is to get a list of all stocks and then analyze it through these 2 indicators. This will give you the list of stocks that have grossed the maximum with these 2 indicators.

# MACD
Here are some of the links that you can refer to for your understanding.

## Pre-requisites
### Exponential Moving Average:
[Link-1](https://www.youtube.com/watch?v=h5NEUq1zXjg) \
[Link-2](https://www.youtube.com/watch?v=PAiSQcd-MrY) \
[Link-3](https://www.youtube.com/watch?v=M9qAEy16kn0) 

### Support and Resistance
[Link-1](https://www.youtube.com/watch?v=B8-_Mxz0Weg&t=27s) \
[Link-2](https://www.youtube.com/watch?v=mi1Po3gatWE) 

## MACD Links
[Link-1](https://www.youtube.com/watch?v=kTo9_aufCtY) \
[Link-2](https://www.youtube.com/watch?v=eob4wv2v--k) \
[Link-3](https://www.youtube.com/watch?v=E3KP1WyLITY) 

## Tradingview add MACD
[Link](https://www.youtube.com/watch?v=udyjrYXZTTI) 

# RSI
Here are some of the links that you can refer to for your understanding.

## RSI Links
[Link-1](https://www.youtube.com/watch?v=rgVdgR1y1Dg) \
[Link-2](https://www.youtube.com/watch?v=VH84ppzmq9Q) \
[Link-3](https://www.youtube.com/watch?v=cjoEGsB7ph4) 

## Add RSI to tradingview
Check Link-3 in the above list. He explains how to do it.

# Fixing the json issue with yfinance
In /usr/local/lib/python3.8/dist-packages/yfinance/base.py Make below changes:
        # Getting data from json
        num_tries = 4
        url = "{}/v8/finance/chart/{}".format(self._base_url, self.ticker)
        while num_tries > 0:
            data = self.session.get(
                url=url,
                params=params,
                proxies=proxy,
                headers=utils.user_agent_headers
            )
            if "Will be right back" in data.text:
                raise RuntimeError("*** YAHOO! FINANCE IS CURRENTLY DOWN! ***\n"
                               "Our engineers are working quickly to resolve "
                               "the issue. Thank you for your patience.")
            if data.status_code == 404:
                _time.sleep(0.5)
                num_tries -= 1
                continue
            else:
                data = data.json()
                break
        if num_tries == 0:
            print("%s failed to get information"%self.ticker)
            data = {}


# Exaplaining this project
1. Get list fo all the tickers from last 2 months(30min data).\
2. For Each ticker:\
    a. Calculate the MACD and RSI starting from index 35 (Why? bcz MACD is 26 and 9).\
    b. If MACD line has crossed signal line and RSI is more than 57%, get that as interval-start index.\
    c. Now ignore RSI and check when the MACD goes below signal. That is your interval-end index.\
    d. In this interval, calculate the amount of highest and lowest you got. Highest that you got is the profit.\
    e. Find out all such intervals when MACD is more than signal(Ignore RSI then).\
    f. Calculate the average profit percentage of all such intervals. i.e If there are 3 intervals with profit percentage of 5%, 3% and 7%, the Avg_ profit = 5% \
    g. Sort this list based on the Profit Percentage Average calculated in f. \
3. Output the list \
4. Send the output to your email id \

# Implementation and Solving Memeory issue
There were many issues with memory. Observations were that about 500 tickers take 4GB of Memory. So, for 7000 tickers, it takes 56GB. Now on your VM with 5-8GB, how do you run this code.
Options:
1. Move to Cloud. You might have to pay 100-300$ per month.
2. Use Swap memory of 64GB that is on the disk and direct the program to use it. But, if you use it, then make sure it is not your personal laptop :) . Bcz, your nvme/SSD will get screwed in some time.
3. Process 500 tickers at a time. I have used this option.

## Using Forks and Pickle to solve memory issue
1. If you are processing for today on March 2nd. You need to get the samples of the stock from Jan 1st to March 1st. Pass it through the Technical Analaysis Library for creating history. We do not need to do this Analysis on March 2nd. We can even do it before hand.
2. On March 1st evening, we can get the info for all the 7000 tickers in the batch of 500 tickers, pass them through techinical analysis class and create an object. This object can be stored on the filesystem. Yes, you can stroe an object as a file using PICKLE class.
3. When you start the Analysis of the tickers on 2nd March, you need to download just that day's sample's and pass them through the technical analysis class, which already has the inforation about prevous samples that was stored earlier as a file.
4. Now, only 500 tickers are run at a time.
