from urllib.request import urlopen, Request
from bs4 import BeautifulSoup
import os
import sys
import pandas as pd
import yfinance as yf
from stockbaba_lib import url_scraper
import json
sys.path.append(os.environ["POLYGON_LIB_DIR"])
import polylibs
import nltk
from nltk.sentiment import SentimentIntensityAnalyzer
from nltk.tokenize import sent_tokenize
import asyncio
import aiohttp

class stock_news():
    def __init__(self, ticker, max_links, output_dir = "output"):
        self.finwiz_url = 'https://finviz.com/quote.ashx?t=' + ticker
        self.ticker = ticker
        req = Request(url=self.finwiz_url, headers={'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'})
        response = urlopen(req)
        html = BeautifulSoup(response, "lxml")
        news_t = html.find(id='news-table')
        self.news_table = news_t.findAll('tr')
        self.max_links = max_links
        self.news_texts = []
        self.timeout = aiohttp.ClientTimeout(total=0)
        self.poly = polylibs.polygon_lib(apikey = os.environ["POLYGON_IO_API_KEY"])
        self.info = self.poly.get_one_ticker(ticker)
        if 'results' in self.info and 'splits' in self.info['results']:
            self.p_splits = self.info['results']['splits']
        else:
            self.p_splits = 0
            
        self.url_list = []
        self.title = []
        self.responses = []
        date_list = []
        time_list = []
        #Get the news text
        for i, table_row in enumerate(self.news_table):
            if i == self.max_links:
                break
            date_array = table_row.td.text.split()
            if len(date_array) == 1:
                time_t = date_array[0]
            else:
                date_t = date_array[0]
                time_t = date_array[1]
            
            date_list.append(date_t)
            time_list.append(time_t)
            self.url_list.append(table_row.a['href'])
            self.title.append(table_row.a.text)

        asyncio.run(self.get_asyncio_response())
        for i, ele in enumerate(self.url_list):
            scraper = url_scraper.web_scraper(url = ele, title = self.title[i], response = self.responses[i], date_t = date_list[i], time_t = time_list[i])
            scraper.get_article_text_from_url()
            self.news_texts.append(scraper)

        curr_dir = os.getcwd()
        self.json_file = "%s/%s/stock_news.json"%(curr_dir, output_dir)
        self.output_file = "%s/%s/stock_news_output_%s"%(curr_dir, output_dir, ticker)
        self.json_text_link = "%s/%s/stock_news_article_%s"%(curr_dir, output_dir, ticker)

    async def get_asyncio_response(self):
        tasks = []
        connector = aiohttp.TCPConnector(limit=10000, limit_per_host=10000)
        session = aiohttp.ClientSession(timeout = self.timeout, connector = connector)
        for ele in self.url_list:
            url = ele
            tt = session.get(url, headers={'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'})
            if tt:
                tasks.append(tt)

        try:
            responses = await asyncio.gather(*tasks)
        except aiohttp.client_exceptions.ClientOSError as e:
            print("Exception Occured for Internet")
            time.sleep(2)
            await session.close()
            del session
            del connector
            return
        for response in responses:
            self.responses.append(await response.text())
            if response.status != 200:
                print("Got Incorrect response status: %d "%(response.status))
        await session.close()
        del responses
        del session

    def __del__(self):
        del self.finwiz_url
        del self.ticker
        del self.news_table
        del self.max_links
        del self.news_texts
        #del self.text_file
        #del self.output_file

    def text_sentiment(self, text):
        sid = SentimentIntensityAnalyzer()
        ret = sid.polarity_scores(text)
        if 'compound' in ret:
            del sid
            return ret['compound']
        else:
            return 0

    def check_sentiment_tokenized(self, paragraph, words):
        # Tokenize the paragraph into sentences
        sentences = sent_tokenize(paragraph)
    
        # Initialize the sentiment analyzer
        sid = SentimentIntensityAnalyzer()
        target_sentence = ""
    
        for sentence in sentences:
            if any([x in sentence for x in words]):
                target_sentence += sentence
    
        # Perform sentiment analysis on the target sentence
        if target_sentence:
            sentiment_scores = sid.polarity_scores(target_sentence)
            return sentiment_scores['compound']
        else:
            return 0

    

    def print_info(self):
        out_file = open(self.output_file, 'w')
        out_file.write("============= %s ==============\n"%self.ticker)
        for scraper in self.news_texts:
            sys.stdout.flush()
            out_file.write("\n######################\n")
            out_file.write("URL: %s\n" %scraper.url)
            out_file.write("The Article is as below\n")
            if scraper.text != None:
                out_file.write(scraper.text)
                out_file.write(scraper.html)
        out_file.close()

    def get_url_json(self, url, date_t, time_t, title, t_sentiment, b_sentiment, c_sentiment, body):
        json_url_info = {
            'url': url,
            'article_title': title,
            'date': date_t,
            'time': time_t,
            'title_sentiment': t_sentiment,
            'full_body_sentiment': b_sentiment,
            'company_sentiment': c_sentiment,
            'body': body
        }
        return json_url_info

    def print_json(self):
        try:
            company_name = self.info["results"]["name"]
        except:
            company_name = 'No Info'
        try:
            industry_name = self.info["results"]['sic_description']
        except:
            industry_name = 'No Info'

        json_main_info = {
            'ticker': self.ticker,
            'company_name': company_name,
            'industry': industry_name,
            'num_positive_splits': self.p_splits,
            'finviz_url': self.finwiz_url,
            'all_url_infos': []
        }
        num_company_keys = 0
        company_key_senti = 0
        company_key = company_name.split(' ')[0]
        senti = 0
        for i, scraper in enumerate(self.news_texts):
            t_sentiment = self.text_sentiment(scraper.title)
            b_sentiment = self.text_sentiment(scraper.text)
            c_sentiment = self.check_sentiment_tokenized(scraper.text, [company_name.split(' ')[0]])
            if company_key in scraper.title:
                num_company_keys += 1
                company_key_senti += b_sentiment
            senti += b_sentiment
            json_url_info = self.get_url_json(url = scraper.url, date_t = scraper.date, time_t = scraper.time, title = scraper.title, \
                            t_sentiment = t_sentiment, b_sentiment = b_sentiment, 
                            c_sentiment = c_sentiment, body = scraper.text)
            json_main_info['all_url_infos'].append(json_url_info)
        json_main_info['num_trl_infos'] = len(json_main_info['all_url_infos'])
        json_main_info['key_sentiment'] = company_key_senti/num_company_keys
        json_main_info['average_sentiment'] = senti/len(json_main_info['all_url_infos'])
        with open(self.json_file, 'w') as outfile:
            json.dump(json_main_info, outfile, indent=2)
        del json_main_info
            
            
