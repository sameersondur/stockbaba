#!/bin/python
import smtplib, ssl
from email.mime.text import MIMEText
import base64
from base64 import b64encode
from email.mime.multipart import MIMEMultipart

class gmail():
    def __init__(self, sender_mail, passwd_file, port = 587, smtp_server = "smtp.gmail.com"):
        self.file = open(passwd_file, "r")
        self.passwd = self.file.read()
        self.file.close()
        #self.message = """Subject: Hi there
        #                testing This message is sent from Python."""
        self.message = None
        self.context = ssl.create_default_context()
        self.smtp_server = smtp_server
        self.port = port
        self.sender_mail = sender_mail
        self.receiver = []
        self.server = None

    def create_message(self, sender, to, subject, message_text):
        """Create a message for an email.
        
        Args:
          sender: Email address of the sender.
          to: Email address of the receiver.
          subject: The subject of the email message.
          message_text: The text of the email message.
        
        Returns:
          An object containing a base64url encoded email object.
        """
        message = MIMEMultipart("alternative") 
        message['To'] = to
        message['From'] = sender
        message['Subject'] = subject
        msg = MIMEText(message_text, "html")
        message.attach(msg)
        self.message = message.as_string()
        #self.message = {'raw': base64.urlsafe_b64encode(message.as_bytes())}

    def add_receiver(self, receiver):
        if receiver in self.receiver:
            return
        self.receiver.append(receiver)

    def remove_receiver(self, receiver):
        if receiver not in self.receiver:
            return
        self.receiver.remove(receiver)

    def send_single_mail(self, receiver, message):
        self.add_receiver(receiver = receiver)
        self.send_mail(message = message)
        self.remove_receiver(receiver = receiver)

    def send_mail(self, message = None, subject = "Sentiquity Info"):
        if len(self.receiver) == 0:
            return
        for mail_id in self.receiver:
            if self.server != None:
                del self.server
            if message == None:
                self.create_message(sender = self.sender_mail, to = mail_id, subject = "Testing the shit", message_text = "There is something wrong")
            else:
                self.create_message(sender = self.sender_mail, to = mail_id, subject = subject, message_text = message)
            with smtplib.SMTP(host = self.smtp_server, port = self.port) as self.server:
                self.server.connect(host = self.smtp_server, port = self.port)
                self.server.ehlo()
                self.server.starttls(context = self.context)
                self.server.ehlo()
                self.server.login(self.sender_mail, self.passwd)
                self.server.sendmail(self.sender_mail, mail_id, self.message)
    
            
class mail_content():
    def not_subscribed(self):
        return "You are not subscribed to the service. Please subscribe to the service by filling the google form with \"Add the below tickers to my list\" as the Option"

    def ticker_list_request(self, tickers):
        return "Below is the List of Tickers that you have subscribed for:\n%s"%tickers

    def unsubscribed_success(self):
        return "You have successfully UNSUBSCRIBED"
